﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteChanger : MonoBehaviour {

	// Use this for initialization
	void Start () {
        ChangeScript.change = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space) ){ 
            ChangeScript.change = Random.Range(-2, 2);
        }
    }
}
