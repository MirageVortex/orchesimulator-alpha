﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMove : MonoBehaviour {

    private Vector3 movement;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
    void Update()
    {
        movement = Vector3.zero;

        if (Input.GetKey("w"))
        {
            movement += Vector3.forward;
        }
        if (Input.GetKey("s"))
        {
            movement += Vector3.back;
        }
        if (Input.GetKey("a"))
        {
            movement += Vector3.left;
        }
        if (Input.GetKey("d"))
        {
            movement += Vector3.right;
        }
        transform.position += movement;
    }
}
