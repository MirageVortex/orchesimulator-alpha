﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class emailTemplateList : MonoBehaviour {

    public static string sponsorshipEmail = "{2} {0}," + "\n\n" +
        "As a representative of the City Council, I'm happy to inform you that your orchestra has been selected as part of the Classical Music Initiative that the city voted on last year in order to promote both the awareness and appreciation of the classical music! As such, if you can prepare a performance by the end of the month, we would be more than happy to fund all of its expenditures, including but not limited to items such as the performer's pay and the venue. We hope to see a spectacular concert!" + "\n\n" +
        "Best," + "\n" +
        "{1}" + "\n" +
        "Secretary of the City Council"; 

    public string getSponsorshipEmail() {
        return sponsorshipEmail;
    }

    public static string concertHallEmail = "Greetings {0}," + "\n\n" +
        "First of all, congratulations on being recently selected by the City Council! We are excited to announce that we have been designated for your performance later this month. The time and date we have available for you is {3}. Please respond within the next two business days if you would like us to reserve a spot for you." + "\n" +
        "Additionally, for future performances, please contact us at least a month in advance with a specific time and date, and we will do our best to accommodate your ensemble." + "\n\n" +
        "Best," + "\n" +
        "{1}" + "\n" +
        "Manager of {2}";

    public string getConcertHallEmail() {
        return concertHallEmail;
    }

    public static string acceptEmail = "Greetings {0}," + "\n\n" +
        "We have received your confirmation and have scheduled you for {3}. We plan to price admission at ${4}.00 per ticket, and are looking to fill our {5} seats. We look forward to hearing your performance on that date." + "\n\n" +
        "Best," + "\n" +
        "{1}" + "\n" +
        "Manager of {2}";

    public string getAcceptEmail() {
        return acceptEmail;
    }

    public static string rejectEmail = "Greetings {0}," + "\n\n" +
        "Thank you for your response, and we hope that you'll be interested in performing at {2} some other time." + "\n\n" +
        "Best," + "\n" +
        "{1}" + "\n" +
        "Manager of {2}";

    public string getRejectEmail() {
        return rejectEmail;
    }

    public static string noAnswerEmail = "Greetings {0}," + "\n\n" +
        "Unfortunately, as we did not receive your response in a timely fashion, we are unfortunately unable to continue our offer for your ensemble to perform in our hall. Our busy schedule forces us to make these restraints, and we hope you will understand." + "\n\n" +
        "Best," + "\n" +
        "{1}" + "\n" +
        "Manager of {2}";

    public string getNoAnswerEmail() {
        return noAnswerEmail;
    }

    public static string concertReviewMascotGreat = "My Friend {0}," + "\n\n" +
        "Last night was an amazing performance. It's really just unbelievable to see how far this group has come. I remember like it was yesterday when the orchestra started and you were just a wee toddler... These have been some good times. Anyways, I impatiently await you to schedule our next big hit." + "\n\n" +
        "{1}";

    public string getConcertReviewMascotGreat() {
        return concertReviewMascotGreat;
    }

    public static string concertReviewJournalistGreat = "Maestro {0}," + "\n\n" +
        "Your performance was truly a wonder yesterday evening. The manner in which your orchestra played {3} moved me to tears. Really, I was overcome with emotions and felt that even a standing ovation didn't do it justice. I hope that you might also read my lengthy review in the upcoming newspaper, and I hope it inspires others to attend your future concerts. I know that I will." + "\n\n" +
        "{1}" + "\n" +
        "Journalist for {2}";

    public string getConcertReviewJournalistGreat() {
        return concertReviewJournalistGreat;
    }

    public static string concertReviewNewsGreat = "{4} {0}," + "\n\n" +
        "Remarkable. Absolutely remarkable. The way the orchestra sang was nothing short of such. I've been to countless performances, and your's might have taken the cake as one of my favories. I'm so impressed by the quality you have managed to develop, and I ancticipate nothing but greater in the future from such a captivating director." + "\n\n" +
        "Best regards," + "\n" +
        "{1} of {2}";

    public string getConcertReviewNewsGreat() {
        return concertReviewNewsGreat;
    }

    public static string concertReviewHallGreat = "Greetings {0}," + "\n\n" +
        "You certainly did not disappoint with last night's performance. The crowd went wild, and the reviews were great. We're honored that you chose to perform at our venue, and we hope that you will continue your patronage in the future. Thank you very much, and we look forward to hearing from you again." + "\n\n" +
        "Best," + "\n" +
        "{1}" + "\n" +
        "Manager of {2}";

    public string getConcertReviewHallGreat() {
        return concertReviewHallGreat;
    }

    public static string concertReviewNewsGood = "{4} {0}," + "\n\n" +
        "Congratulations on last night's performance. It was splendid to listen to, and I look forward to seeing the next one. I wish your ensemble the best of luck." + "\n\n" +
        "Sincerely," + "\n" +
        "{1} of {2}";

    public string getConcertReviewNewsGood() {
        return concertReviewNewsGood;
    }

    public static string concertReviewMascotGood = "My Friend {0}," + "\n\n" +
        "A great performance indeed. With shows like these, I'm sure the hall is left very pleased. Your aspirations of running a top level orchestra are in sight (at least through a monocle with telescopic properties like mine). Let's keep working towards this goal of a concert for all 7 billion people in the world! (Was that right?)" + "\n\n" +
        "{1}";

    public string getConcertReviewMascotGood() {
        return concertReviewMascotGood;
    }

    public static string concertReviewJournalistGood = "Maestro {0}," + "\n\n" +
        "I quite enjoyed last night's concert. I felt it a quality performance and it garnered my interest to see your future performances. Perhaps with a bit more polishing though, you could truly captivate another's soul. If my sight into your potential is right, then I eargerly anticipate that moment." + "\n\n" +
        "{1}" + "\n" +
        "Journalist of {2}";

    public string getConcertReviewJournalistGood() {
        return concertReviewJournalistGood;
    }

    public static string concertReviewHallGood = "Greetings {0}," + "\n\n" +
        "The performance was well received. With these reviews, we would be happy to have you perform another time. While we are still a busy hall, if you send us an advanced notice, we'll be sure to reserve a spot for your next performance." + "\n\n" +
        "Best," + "\n" +
        "{1}" + "\n" +
        "Manager of {2}";

    public string getConcertReviewHallGood() {
        return concertReviewHallGood;
    }

    public static string concertReviewMascotBad = "My Friend {0}," + "\n\n" +
        "Perhaps that could have gone a litte better, wouldn't you say? Ah well, things happen as they do, but the audience might not have been too satisfied with how their money was spent. It might mean a smaller crowd for next time, but we'll pull through. That being said, perhaps we should try something else for the next performance." + "\n\n" +
        "{1}";

    public string getConcertReviewMascotBad() {
        return concertReviewMascotBad;
    }

    public List<string> getReviewEmailsGreat() {
        List<string> reviewEmailsGreat = new List<string>() {
            concertReviewMascotGreat,
            concertReviewHallGreat,
            concertReviewNewsGreat,
            concertReviewJournalistGreat
        };

        return reviewEmailsGreat;
    }

    public List<string> getReviewEmailsGood() {
        List<string> reviewEmailsGood = new List<string>() {
            concertReviewMascotGood,
            concertReviewHallGood,
            concertReviewNewsGood,
            concertReviewJournalistGood
        };

        return reviewEmailsGood;
    }

    public List<string> getReviewEmailsBad() {
        List<string> reviewEmailsBad = new List<string>() {
            concertReviewMascotBad
        };

        return reviewEmailsBad;
    }
}
