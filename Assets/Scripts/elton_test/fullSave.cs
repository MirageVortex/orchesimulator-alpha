﻿using System.IO;
using UnityEngine;

public class fullSave : MonoBehaviour {

    public void permSaveFunction() {



        // load perm save files
        string permCharaSave = Application.streamingAssetsPath + "/Save"+globalVariables.savefile+ "/CharaStats.json";
        string permSaveSlot = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/SaveState.json";
        string permEmailSave = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/Email.json";
        string permLibrarySave = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/Library.json";
        string permPlayerSave = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/Player.json";
        string permDateSave = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/Date.json";

        // load temp save files
        string tempSaveSlot = Application.streamingAssetsPath + "/Temp/TempSaveState.json";
        string saveSlot = Application.streamingAssetsPath + "/SaveState.json";
        string tempCharaSave = Application.streamingAssetsPath + "/Temp/CharaStats.json";
        string tempEmailSave = Application.streamingAssetsPath + "/Temp/Email.json";
        string tempLibrarySave = Application.streamingAssetsPath + "/Temp/Library.json";
        string tempPlayerSave = Application.streamingAssetsPath + "/Temp/Player.json";
        string tempDateSave = Application.streamingAssetsPath + "/Temp/Date.json";

        // read the temp save files
        string originalSaveString = File.ReadAllText(saveSlot);
        string saveSaveString = File.ReadAllText(tempSaveSlot);
        string charaSaveString = File.ReadAllText(tempCharaSave);
        string emailSaveString = File.ReadAllText(tempEmailSave);
        string librarySaveString = File.ReadAllText(tempLibrarySave);
        string playerSaveString = File.ReadAllText(tempPlayerSave);
        string dateSaveString = File.ReadAllText(tempDateSave);



        saveStateClass.SaveState maxSaveState = JsonUtility.FromJson<saveStateClass.SaveState>(originalSaveString);
        saveStateClass.SaveState currentSaveState = JsonUtility.FromJson<saveStateClass.SaveState>(saveSaveString);
        if (currentSaveState.SaveStateValue > maxSaveState.SaveStateValue)
        {
            File.WriteAllText(saveSlot, saveSaveString);

        }

        // write over the new files
        File.WriteAllText(permSaveSlot, saveSaveString);
        File.WriteAllText(permCharaSave, charaSaveString);
        File.WriteAllText(permEmailSave, emailSaveString);
        File.WriteAllText(permLibrarySave, librarySaveString);
        File.WriteAllText(permPlayerSave, playerSaveString);
        File.WriteAllText(permDateSave, dateSaveString);
    }
}
