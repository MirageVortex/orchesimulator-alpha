﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class npcClass : MonoBehaviour
{
    private int npcNum = 3;

    [System.Serializable]
    public class NPC {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public buildingClass.Building Company { get; set; }
        // 0 - Mayor || 1 - Concert Hall 1 Owner || 2 - Practice Hall Owner || 3 - (more will be created later)

        public NPC(string firstName, string lastName, buildingClass.Building company) {
            FirstName = firstName;
            LastName = lastName;
            Company = company;
        }
    }
}