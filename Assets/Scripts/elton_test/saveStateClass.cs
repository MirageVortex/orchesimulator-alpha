﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class saveStateClass : MonoBehaviour {

    [System.Serializable]
    public class SaveState
    {
        public int SaveStateValue;
    }
}
