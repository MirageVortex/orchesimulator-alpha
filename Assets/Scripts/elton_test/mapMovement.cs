﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class mapMovement : MonoBehaviour {

    public GameObject enterbutton;
    private Vector3 scale;
    private enterBuilding eb;
    private Dictionary<string, string> buildingMovementDict;
    public string buildingName;
    public string sceneName;
    private string[] buildingNames = { "concertHall1", "practiceHall", "playerHome1" };
    private string[] sceneNames = { "recital_hall", "practice_hall", "playerHome_elton" };
    private string building;
	// Use this for initialization
	void Start () {
        scale = transform.localScale;
        eb = enterbutton.GetComponent<enterBuilding>();
        buildingMovementDict = new Dictionary<string, string>();
        for (int i = 0; i < buildingNames.Length; i++) {
            buildingMovementDict.Add(buildingNames[i], sceneNames[i]);
        }
	}

    private void OnMouseEnter() {
        transform.localScale = scale * 1.2f;
    }

    private void OnMouseExit() {
        transform.localScale = scale;
    }

    private void OnMouseDown() {
        

        if (enterbutton.activeSelf == false || eb.GetName() != buildingName) {
            eb.goodnight.enabled = false;
            eb.practiceHallClosed.enabled = false;
            eb.concertHallClosed.enabled = false;
            if (globalVariables.time == 3 && buildingName!= "Player Home") {
                eb.button.SetActive(false);
                eb.goodnight.enabled = true;
            } else if (globalVariables.dayEvent != 1 && buildingName == "Practice Hall") {
                eb.button.SetActive(false);
                eb.practiceHallClosed.enabled = true;
            } else if (globalVariables.dayEvent != 3 && buildingName == "Concert Hall") {
                eb.button.SetActive(false);
                eb.concertHallClosed.enabled = true;
            } else {
                eb.button.SetActive(true);
                eb.goodnight.enabled = false;
            }
            building = buildingName;
            eb.SetName(building, sceneName);
            enterbutton.SetActive(true);
            float right = ((Screen.width - Input.mousePosition.x) / Screen.width) * 2.0f - 1.0f;
            float up = ((Screen.height - Input.mousePosition.y) / Screen.height) * 2.0f - 1.0f;

            if (right >=0 && up>=0) 
                enterbutton.transform.position= Input.mousePosition+Vector3.up*80 + Vector3.right*100;
            else if (right >=0 && up<0) {
                enterbutton.transform.position = Input.mousePosition + Vector3.down * 80 + Vector3.right * 100;
            }
            else if (right <0 && up>=0) {
                enterbutton.transform.position = Input.mousePosition + Vector3.up * 80 + Vector3.left * 100;
            }
            else if (right <0 && up < 0) {
                enterbutton.transform.position = Input.mousePosition + Vector3.down * 80 + Vector3.left * 100;
            }
        }

        //SceneManager.LoadScene(buildingMovementDict[building]);
    }

    public void exitBuilding() {
        //Debug.Log(globalVariables.time);
        //if (globalVariables.time < 3) {
            SceneManager.LoadScene("WorldMap");
        //    if (globalVariables.time < 3 && globalVariables.daystart == true) {
        //        globalVariables.time++;
        //    }
        //    else
        //        globalVariables.daystart = true;
        //}
        
    }
}
