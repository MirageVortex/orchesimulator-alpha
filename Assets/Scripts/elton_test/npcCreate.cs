﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

public class npcCreate : MonoBehaviour {
    public GameObject itemList;
    public GameObject statsFunct;

    private string[] maleNames;
    private string[] femaleNames;
    private string[] lastNames;

    public List<npcClass.NPC> npcList;
    private List<buildingClass.Building> buildingList;

    // Use this for initialization
    void Start() {
        maleNames = itemList.GetComponent<itemList>().MaleNamesList;
        femaleNames = itemList.GetComponent<itemList>().FemaleNameList;
        lastNames = itemList.GetComponent<itemList>().LastNameList;

        string path = Application.streamingAssetsPath + "/Save1/Buildings.json";
        string jsonString = File.ReadAllText(path);

        buildingList = JsonConvert.DeserializeObject<List<buildingClass.Building>>(jsonString);

        for (int i = 0; i < buildingList.Count; i++) {
            int gender = Random.Range(0, 2);
            if (gender == 0) {
                npcList.Add(new npcClass.NPC(maleNames[Random.Range(0, 101)], lastNames[Random.Range(0, 26)], buildingList[i]));
            } else {
                npcList.Add(new npcClass.NPC(femaleNames[Random.Range(0, 101)], lastNames[Random.Range(0, 26)], buildingList[i]));
            }
        }

        npcSave();
    }

    public void npcSave() {
        statsFunct.GetComponent<statsFunctions>().npcJSONSave(npcList);
    }
}
