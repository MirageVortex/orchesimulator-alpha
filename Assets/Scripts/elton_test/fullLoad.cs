﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class fullLoad : MonoBehaviour {

    public GameObject savefileholder;

    public void loadGameData() {

        int savefile = savefileholder.GetComponent<Dropdown>().value+1;

        // load perm save files
        string permCharaSave = Application.streamingAssetsPath + "/Save" + savefile + "/CharaStats.json";
        string permSaveSlot = Application.streamingAssetsPath + "/Save" + savefile + "/SaveState.json";
        string permEmailSave = Application.streamingAssetsPath + "/Save" + savefile + "/Email.json";
        string permLibrarySave = Application.streamingAssetsPath + "/Save" + savefile + "/Library.json";
        string permPlayerSave = Application.streamingAssetsPath + "/Save" + savefile + "/Player.json";
        string permDateSave = Application.streamingAssetsPath + "/Save" + savefile + "/Date.json";

        // load temp save files
        string tempCharaSave = Application.streamingAssetsPath + "/Temp/CharaStats.json";
        string tempSaveSlot = Application.streamingAssetsPath + "/Temp/TempSaveState.json";
        string tempEmailSave = Application.streamingAssetsPath + "/Temp/Email.json";
        string tempLibrarySave = Application.streamingAssetsPath + "/Temp/Library.json";
        string tempPlayerSave = Application.streamingAssetsPath + "/Temp/Player.json";
        string tempDateSave = Application.streamingAssetsPath + "/Temp/Date.json";





        // read the perm save files
        string saveSaveString = File.ReadAllText(permSaveSlot);
        string charaSaveString = File.ReadAllText(permCharaSave);
        string emailSaveString = File.ReadAllText(permEmailSave);
        string librarySaveString = File.ReadAllText(permLibrarySave);
        string playerSaveString = File.ReadAllText(permPlayerSave);
        string dateSaveString = File.ReadAllText(permDateSave);

        // write over the new files
        File.WriteAllText(tempSaveSlot, saveSaveString);
        File.WriteAllText(tempCharaSave, charaSaveString);
        File.WriteAllText(tempEmailSave, emailSaveString);
        File.WriteAllText(tempLibrarySave, librarySaveString);
        File.WriteAllText(tempPlayerSave, playerSaveString);
        File.WriteAllText(tempDateSave, dateSaveString);

        globalVariables.savefile = savefile;
    }
}
