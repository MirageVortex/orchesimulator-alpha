﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class emailCreate : MonoBehaviour {

    // Public variables
    public GameObject statsFunct;
    public GameObject ItemList;
    public GameObject calendarScript;

    public emailTemplateList emailTemplates;

    private string[] instruments;

    // Data List
    private List<playerClass.Player> playerList;
    private List<npcClass.NPC> npcList;
    private List<LibraryClass.Repertoire> repertoireList;

    // Email Data
    private emailClass.Email firstEmail;

    // Use this for initialization
    void Start () {
        // initialize the player list
        string playerPath = Application.streamingAssetsPath + "/Temp/Player.json";
        string playerJsonString = File.ReadAllText(playerPath);

        playerList = JsonConvert.DeserializeObject<List<playerClass.Player>>(playerJsonString);

        // initalize the npc list
        string npcPath = Application.streamingAssetsPath + "/Save" + (globalVariables.savefile+1) + "/NPC.json";
        string npcJsonString = File.ReadAllText(npcPath);

        npcList = JsonConvert.DeserializeObject<List<npcClass.NPC>>(npcJsonString);

        // initalize the repertoire list
        string repertoirePath = Application.streamingAssetsPath + "/Temp/Library.json";
        string repertoireJsonString = File.ReadAllText(repertoirePath);

        repertoireList = JsonConvert.DeserializeObject<List<LibraryClass.Repertoire>>(repertoireJsonString);

        // if first email is not in the list
        string emailPath = Application.streamingAssetsPath + "/Temp/Email.json";
        string emailJsonString = File.ReadAllText(emailPath);

        if (emailJsonString == "") {
            globalVariables.emailList = new List<emailClass.Email>();
            string sponsorshipEmailTemplate = emailTemplates.getSponsorshipEmail();

            emailClass.Email email = sponorshipEmail(emailTemplateList.sponsorshipEmail, playerList[0], npcList[0]);
            globalVariables.emailList.Add(email);

            statsFunct.GetComponent<statsFunctions>().emailJSONSave(globalVariables.emailList, 1);
        } else {
            // initialize the email list
            globalVariables.emailList = JsonConvert.DeserializeObject<List<emailClass.Email>>(emailJsonString);
        }
    }

    void Update()
    {
        if (globalVariables.dt.AddDays(globalVariables.day) == new DateTime(2017, 8, 2) && globalVariables.emailList.Count == 1) {
            string concertHallFirstRequest = emailTemplates.getConcertHallEmail();
            globalVariables.emailList.Add(concertHallBookingEmail(concertHallFirstRequest, playerList[0], npcList[1], 0, "Peformance Opening"));
        } else if (globalVariables.dt > new DateTime(2017, 8, 2) && globalVariables.emailList.Count < 3) {
            string concertHallNoReply = emailTemplates.getNoAnswerEmail();
            globalVariables.emailList.Add(concertHallBookingEmail(concertHallNoReply, playerList[0], npcList[1], 0, "Opening Withdrawal"));
        }
    }

    emailClass.Email concertHallBookingEmail(string emailBody, playerClass.Player player, npcClass.NPC npc, int type, string subject) {
        string recipientName = player.FirstName + " " + player.LastName;
        string senderName = npc.FirstName + " " + npc.LastName;
        string companyName = npc.Company.Name;
        string date = globalVariables.dt.AddDays(globalVariables.day).ToShortDateString();
        string performanceDate = new DateTime(2017, 8, 31).ToShortDateString();
        string ticketPrice = npc.Company.TicketPrice.ToString();
        string seatNum = npc.Company.SeatNum.ToString();

        string[] parameterList = new string[] { recipientName, senderName, companyName, performanceDate, ticketPrice, seatNum };

        string emailContent = string.Format(emailBody, parameterList);

        emailClass.Email newEmail = new emailClass.Email(senderName, recipientName, type, subject, date, emailContent);

        return newEmail;
    }

    emailClass.Email concertReviewEmail(string emailBody, playerClass.Player player, npcClass.NPC npc, LibraryClass.Repertoire repertoire) {
        string recipientName = player.FirstName + " " + player.LastName;
        string senderName = npc.FirstName + " " + npc.LastName;
        string companyName = npc.Company.Name;
        string date = globalVariables.dt.AddDays(globalVariables.day).ToShortDateString();
        string piece = repertoire.Title;
        string greeting;
        if (player.Gender == 0) {
            greeting = "Mr.";
        } else {
            greeting = "Ms.";
        }

        string[] parameterList = new string[] { recipientName, senderName, companyName, piece, greeting };

        string emailContent = string.Format(emailBody, parameterList);

        emailClass.Email newEmail = new emailClass.Email(senderName, recipientName, 1, "Review", date, emailContent);

        return newEmail;
    }

    emailClass.Email sponorshipEmail (string emailBody, playerClass.Player player, npcClass.NPC npc) {
        string recipientName = player.FirstName + " " + player.LastName;
        print(recipientName);
        string senderName = npc.FirstName + " " + npc.LastName;
        print(senderName);
        string date = globalVariables.dt.AddDays(globalVariables.day).ToShortDateString();
        print(date);
        string greeting;
        if (player.Gender == 0) {
            greeting = "Mr.";
        } else {
            greeting = "Ms.";
        }
        print(greeting);
        string[] parameterList = new string[] { recipientName, senderName, greeting };
        string emailContent = string.Format(emailBody, parameterList);

        emailClass.Email sponsorshipEmail = new emailClass.Email(senderName, recipientName, 1, "Sponsorship", date, emailContent);
        
        return sponsorshipEmail;
    }

    // if player rejects the email for the concert;
    public void acceptEmail () {
        string concertHallAcceptTemplate = emailTemplates.getAcceptEmail();
        globalVariables.emailList.Add(concertHallBookingEmail(concertHallAcceptTemplate, playerList[0], npcList[1], 1, "Confirmation Email"));
        calendarScript.GetComponent<calendarScript>().addPerformanceEvent();
    }

    // if player rejects the email for the concert;
    public void rejectEmail () {
        string concertHallRejectTemplate = emailTemplates.getRejectEmail();
        globalVariables.emailList.Add(concertHallBookingEmail(concertHallRejectTemplate, playerList[0], npcList[1], 1, "Reject Email"));
    }

    public void createReviewEmails(int score) {
        List<string> emailTemplateList;
        if (score == 0) {
            emailTemplateList = emailTemplates.getReviewEmailsBad();
        } else if (score == 3) {
            emailTemplateList = emailTemplates.getReviewEmailsGreat();
        } else {
            emailTemplateList = emailTemplates.getReviewEmailsGood();
        }
        foreach (string emailTemplate in emailTemplateList) {
            print(emailTemplate);
        }

        int[] npcNumList = new int[] { 5, 1, 3, 4};
        print(repertoireList[0].Title);

        for (int i = 0; i < emailTemplateList.Count; i++) {
            globalVariables.emailList.Add(concertReviewEmail(emailTemplateList[i], playerList[0], npcList[npcNumList[i]], repertoireList[0]));
        }
    }

}
