﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class playerMenu : MonoBehaviour {
    public GameObject itemListObj;
    public GameObject statsFunct;

    public GameObject nameText;
    public GameObject genderText;
    public GameObject ageText;
    public GameObject instrumentText;

    private string[] genderList;
    private string[] instrumentList;

    public List<playerClass.Player> playerList;

    // Use this for initialization
    void Start () {
        // Load in current playerList
        string path = Application.streamingAssetsPath + "/Temp/Player.json";
        string jsonString = File.ReadAllText(path);

        playerList = JsonConvert.DeserializeObject<List<playerClass.Player>> (jsonString);

        genderList = itemListObj.GetComponent<itemList>().GenderList;
        instrumentList = itemListObj.GetComponent<itemList>().InstrumentList;
    }
	
	// Update is called once per frame
	void Update () {
        UpdateText();
	}

    void UpdateText() {
        nameText.GetComponent<Text>().text = playerList[0].FirstName + " " + playerList[0].LastName;
        genderText.GetComponent<Text>().text = genderList[playerList[0].Gender];
        string age = playerList[0].Age.ToString() + " Years Old";
        ageText.GetComponent<Text>().text = age;
        instrumentText.GetComponent<Text>().text = instrumentList[playerList[0].Instrument];
    }
}
