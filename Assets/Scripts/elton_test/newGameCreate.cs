﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;

public class newGameCreate : MonoBehaviour {
    // used to reset all the temp save files
    public GameObject statFunct;

	// Use this for initialization
	public void newGameStart() {

        // reset global time variables
        globalVariables.datesetup = false;
        globalVariables.rehearsalCount = 0;
        globalVariables.preparedness = 0;
        if (globalVariables.emailList!=null) { 
            globalVariables.emailList.Clear();
        }

        // load temp save files
        string tempSaveSlot = Application.streamingAssetsPath + "/Temp/TempSaveState.json";
        string saveSlot = Application.streamingAssetsPath + "/SaveState.json";

        string saveJsonString = File.ReadAllText(saveSlot);
        saveStateClass.SaveState saveState = JsonUtility.FromJson<saveStateClass.SaveState>(saveJsonString);
        //saveStateClass.SaveState saveState = new saveStateClass.SaveState();
        saveState.SaveStateValue ++;

        string tempCharaSave = Application.streamingAssetsPath + "/Temp/CharaStats.json";
        string tempEmailSave = Application.streamingAssetsPath + "/Temp/Email.json";
        string tempLibrarySave = Application.streamingAssetsPath + "/Temp/Library.json";
        string tempPlayerSave = Application.streamingAssetsPath + "/Temp/Player.json";
        string tempDateSave = Application.streamingAssetsPath + "/Temp/Date.json";


        string libraryDataString = File.ReadAllText(tempLibrarySave);
        List<LibraryClass.Repertoire> libraryData = JsonConvert.DeserializeObject<List<LibraryClass.Repertoire>>(libraryDataString);

        foreach(LibraryClass.Repertoire repertoire in libraryData) {
            repertoire.Purchased = false;
        }

        string newsavestate = JsonConvert.SerializeObject(saveState);
        globalVariables.savefile = saveState.SaveStateValue;

        File.WriteAllText(tempSaveSlot, newsavestate);
        File.WriteAllText(tempCharaSave, "");
        File.WriteAllText(tempEmailSave, "");
        statFunct.GetComponent<statsFunctions>().repitoireJSONSave(libraryData, 1);
        File.WriteAllText(tempPlayerSave, "");
        File.WriteAllText(tempDateSave, "");
    }
        
    
}
