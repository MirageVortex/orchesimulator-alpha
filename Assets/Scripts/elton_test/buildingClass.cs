﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class buildingClass : MonoBehaviour {

    [System.Serializable]
    public class Building
    {
        public string Name { get; set; }
        public string Owner { get; set; }
        public int VenueCost { get; set; }
        public int TicketPrice { get; set; }
        public int SeatNum { get; set; }
        public bool Rented { get; set; }

        public Building(string name, string owner, int venuecost, int ticketprice, int seatnum, bool rented)
        {
            Name = name;
            Owner = owner;
            VenueCost = venuecost;
            TicketPrice = ticketprice;
            SeatNum = seatnum;
            Rented = rented;
        }
    }
}
