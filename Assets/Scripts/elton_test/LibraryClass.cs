﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LibraryClass : MonoBehaviour {

    [System.Serializable]
    public class Repertoire
    {
        public string Title { get; set; }
        public string Composer { get; set; }
        // Corresponds to the InstrumentList in ItemList.cs -- List of Instruments required for this Repitoire
        public int[] InstrumentList { get; set; } 
        // Corresponds to InstrumentList -- the num of instruments required for the Repitoire
        public int[] InstrumentCount { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public bool Purchased { get; set; }

        public Repertoire(string title, string composer, int[] instrumentList, int[] instrumentCount, int price, string description, bool purchased)
        {
            Title = title;
            Composer = composer;
            InstrumentList = instrumentList;
            InstrumentCount = instrumentCount;
            Price = price;
            Description = description;
            Purchased = purchased;
        }
    }
}
