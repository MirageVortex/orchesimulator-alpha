﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newListSave : MonoBehaviour {

    public Transform membersContent;
    public List<charaClass.Character> listToSave;
    public GameObject statFuncts;
	
	public void saveMembers() {
        for (int i=0; i < membersContent.childCount; i++) {
            listToSave.Add(membersContent.GetChild(i).gameObject.GetComponent<npcButton>().npc);
        }

        statFuncts.GetComponent<statsFunctions>().orchestraJSONSave(listToSave, 1);
    }
}
