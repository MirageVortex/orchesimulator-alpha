﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class dateClass : MonoBehaviour {

    [System.Serializable]
    public class Date {
        public int TimeOfDay;
        public int Day;
        public bool DayStart;
        public Dictionary<int, int> ImportantEventsDict;
        public DateTime CalendarDate;
        public int DayEvent;
        public int RehearsalCount;
        
        public Date(int timeOfDay, int day, bool dayStart, Dictionary<int, int> eventsDict, DateTime calendarDate, int dayEvent, int rehearsalCount) {
            TimeOfDay = timeOfDay;
            Day = day;
            DayStart = dayStart;
            ImportantEventsDict = eventsDict;
            CalendarDate = calendarDate;
            DayEvent = dayEvent;
            RehearsalCount = rehearsalCount;
        }
    }
}
