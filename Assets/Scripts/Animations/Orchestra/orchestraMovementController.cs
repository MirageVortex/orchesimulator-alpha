﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orchestraMovementController : MonoBehaviour {

	// All Play
	public bool debugPlay;
	// Strings
	public bool violin1Play;
	public bool violin2Play;
	public bool violaPlay;
	public bool celloPlay;
	public bool bassPlay;
	// Wind
	public bool flutePlay;
	public bool oboePlay;
	public bool clarinetPlay;
	public bool hornPlay;
	public bool bassoonPlay;
	public bool trumpetPlay;
	public bool trombonePlay;
	public bool tbaPlay;
	// Percussion
	public bool timpaniPlay;
	// Conductor
	public bool conductorPlay;
	// Filler
	public bool stockPlay;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
