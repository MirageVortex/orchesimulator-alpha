﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System;
using UnityEngine;
using UnityEngine.UI;

public class playerTestAnimations : MonoBehaviour {

    public GameObject timeManagerScript;

    public GameObject computer;
    public GameObject door;
    public GameObject bed;
    public GameObject piano;
    public Button exitButton;
    public Button sleepButton;
    public Button skipButton;

    public AudioSource enterBedSFX;
    public AudioSource exitBedSFX;
    public AudioSource sitComputerSFX;
    public AudioSource sitPianoSFX;
    public AudioSource standComputerSFX;
    public AudioSource standPianoSFX;

    public Animator playerAnims;
    public Animator chairAnims;
    public Transform player;

    string furniture;

    // bool functions for moving
    // From Door
    bool doorToBed;

    // FromDoor/Bed
    bool doorBedToCompMid;
    bool doorBedToComp;
    bool doorBedToPianoMid;
    bool doorBedToPiano;

    // From Comp
    bool compToBed;
    bool compToCompMid;
    bool compToDoor;
    bool compToPianoMid;
    bool compToPiano;

    // From Bed
    bool bedToDoor;

    // From Piano
    bool pianoToBed;
    bool pianoToDoor;
    bool pianoToComp;
    bool pianoToCompMid;
    bool pianoToMid;

    private Vector3 tracker;

    private Vector3 bedPosition = new Vector3(-40, 1, 50);
    private Vector3 doorPosition = new Vector3(-40, 1, 0);
    private Vector3 compPosition = new Vector3(-2, 1, 13);
    private Vector3 pianoPosition = new Vector3(10, 1, 40);
    private Vector3 beforePianoPosition = new Vector3(-40, 1, 40);
    private Vector3 bedDoorCompBetween = new Vector3(-40, 1, 13);
    private Vector3 pianoCompBetween = new Vector3(-2, 1, 40);

    public float speed;
    private float startTime;
    private float journeyLength;


    // Use this for initialization
    void Start() {
        // Real game speed (uncomment to use in game)
        speed = 12.5f;

        // For testing purposes (speed up the animations)
        // speed = 50f;

        globalVariables.movable = true;

        doorToBed = false;
        doorBedToCompMid = false;
        doorBedToComp = false;
        doorBedToPianoMid = false;
        doorBedToPiano = false;
        compToBed = false;
        compToCompMid = false;
        compToDoor = false;
        compToPianoMid = false;
        compToPiano = false;
        bedToDoor = false;
        pianoToBed = false;
        pianoToDoor = false;
        pianoToComp = false;
        pianoToCompMid = false;
        pianoToMid = false;

    }

    // Update is called once per frame
    void Update() {
        tracker = new Vector3(Mathf.Round(player.position.x), Mathf.Round(player.position.y), Mathf.Round(player.position.z));

        if (tracker == doorPosition) {
            exitButton.interactable = true;
        } else {            exitButton.interactable = false;
        }

        if (tracker == bedPosition) {
            sleepButton.interactable = true;
            if (globalVariables.importantEventsDict.Count != 0) {
                skipButton.interactable = true;
            }          
        } else {
            sleepButton.interactable = false;
            skipButton.interactable = false;
        }



        if (doorToBed) {
            player.LookAt(bedPosition);
            player.position = Vector3.MoveTowards(player.position, bedPosition, speed * Time.deltaTime);
            if (tracker == bedPosition) {
                playerAnims.Play("Armature|Stand_To_Idle");
                doorToBed = false;
                globalVariables.movable = true;
            }
        }
 
        if (doorBedToCompMid) {
            player.LookAt(bedDoorCompBetween);
            player.position = Vector3.MoveTowards(player.position, bedDoorCompBetween, speed * Time.deltaTime);
            if (tracker == bedDoorCompBetween) {
                player.LookAt(compPosition);
                doorBedToComp = true;
                doorBedToCompMid = false;
            }
        }

        if (doorBedToComp) {
            player.position = Vector3.MoveTowards(player.position, compPosition, speed * Time.deltaTime);            if (tracker == compPosition) {
                sitComputerSFX.Play();
                playerAnims.Play("Armature|Stand_To_Chair");
                chairAnims.Play("Armature|chairAnimate1");
                doorBedToComp = false;
                Invoke("setMovableTrue", 4);
            }
        }

        if (doorBedToPianoMid) {
            player.LookAt(beforePianoPosition);
            player.position = Vector3.MoveTowards(player.position, beforePianoPosition, speed * Time.deltaTime);
            if (tracker == beforePianoPosition) {
                doorBedToPianoMid = false;
                doorBedToPiano = true;
            }
        }

        if (doorBedToPiano) {
            player.LookAt(pianoPosition);
            player.position = Vector3.MoveTowards(player.position, pianoPosition, speed * Time.deltaTime);
            if (tracker == pianoPosition) {
                sitPianoSFX.Play();
                playerAnims.Play("Armature|Stand_To_Piano");
                doorBedToPiano = false;
                Invoke("setMovableTrue", 3);
            }
        }

        if (compToBed) {
            player.LookAt(bedPosition);
            player.position = Vector3.MoveTowards(player.position, bedPosition, speed * Time.deltaTime);
            if (tracker == bedPosition) {                playerAnims.Play("Armature|Stand_To_Idle");
                compToBed = false;
                Invoke("setMovableTrue", 1);
            }
        }

        if (compToCompMid) {
            player.position = Vector3.MoveTowards(player.position, bedDoorCompBetween, speed * Time.deltaTime);
            player.LookAt(bedDoorCompBetween);
            if (tracker == bedDoorCompBetween) {
                if (furniture == "Door") {
                    compToDoor = true;
                } else {
                    compToBed = true;
                }
                compToCompMid = false;
            }
        }

        if (compToDoor) {
            player.LookAt(doorPosition);
            player.position = Vector3.MoveTowards(player.position, doorPosition, speed * Time.deltaTime);
            if (tracker == doorPosition) {               
                playerAnims.Play("Armature|Stand_To_Idle");
                compToDoor = false;
                Invoke("setMovableTrue", 1);
            }
        }

        if (compToPianoMid) {
            player.LookAt(pianoCompBetween);
            player.position = Vector3.MoveTowards(player.position, pianoCompBetween, speed * Time.deltaTime);            if (tracker == pianoCompBetween)
            {
                compToPianoMid = false;
                compToPiano = true;
            }
        }

        if (compToPiano) {
            player.LookAt(pianoPosition);
            player.position = Vector3.MoveTowards(player.position, pianoPosition, speed * Time.deltaTime);
            if (tracker == pianoPosition) {
                sitPianoSFX.Play();
                playerAnims.Play("Armature|Stand_To_Piano");
                compToPiano = false;
                Invoke("setMovableTrue", 3);
            }
        }


        if (bedToDoor) {
            player.LookAt(doorPosition);
            player.position = Vector3.MoveTowards(player.position, doorPosition, speed * Time.deltaTime);
            if (tracker == doorPosition) {
                playerAnims.Play("Armature|Stand_To_Idle");
                bedToDoor = false;
                Invoke("setMovableTrue", 1);
            }
        }

        if (pianoToBed) {            player.LookAt(bedPosition);
            player.position = Vector3.MoveTowards(player.position, bedPosition, speed * Time.deltaTime);
            if (tracker == bedPosition) {
                playerAnims.Play("Armature|Stand_To_Idle");
                pianoToBed = false;
                Invoke("setMovableTrue", 1);
            }
        }

        if (pianoToDoor) {
            player.LookAt(doorPosition);
            player.position = Vector3.MoveTowards(player.position, doorPosition, speed * Time.deltaTime);
            if (tracker == doorPosition) {
                playerAnims.Play("Armature|Stand_To_Idle");
                pianoToDoor = false;
                Invoke("setMovableTrue", 1);
            }
        }

        if (pianoToComp) {
            player.LookAt(compPosition);
            player.position = Vector3.MoveTowards(player.position, compPosition, speed * Time.deltaTime);
            if (tracker == compPosition) {
                sitComputerSFX.Play();
                player.LookAt(new Vector3(0, 1, 13));
                playerAnims.Play("Armature|Stand_To_Chair");
                chairAnims.Play("Armature|chairAnimate1");
                pianoToComp = false;
                Invoke("setMovableTrue", 4);
            }
        }

        if (pianoToCompMid) {
            player.LookAt(pianoCompBetween);            player.position = Vector3.MoveTowards(player.position, pianoCompBetween, speed * Time.deltaTime);
            if (tracker == pianoCompBetween)
            {
                pianoToCompMid = false;
                pianoToComp = true;
            }
        }

        if (pianoToMid) {
            player.LookAt(beforePianoPosition);
            player.position = Vector3.MoveTowards(player.position, beforePianoPosition, speed * Time.deltaTime);
            if (tracker == beforePianoPosition) {
                if (furniture == "Door") {
                    pianoToDoor = true;
                } else {                    pianoToBed = true;
                }
                pianoToMid = false;
            }
        }
    }

    private void OnMouseDown() {
        furniture = gameObject.name;
        if (globalVariables.movable && ((tracker == bedPosition && furniture != "Bed") || (tracker == compPosition && furniture != "Computer") || (tracker == doorPosition && furniture != "Door") || (tracker == pianoPosition && furniture != "Piano"))) {
            globalVariables.movable = false;
            playerAnims.Play("Armature|Walk_Cycle");
            if (furniture == "Bed") {
                if (tracker == doorPosition)
                {
                    doorToBed = true;
                }
                else if (tracker == compPosition)
                {
                    standComputerSFX.Play();
                    timeManagerScript.GetComponent<timeManagerPlayerhome>().passTime();
                    playerAnims.Play("Armature|Chair_To_Stand");
                    chairAnims.Play("Armature|chairAnimate2");
                    Invoke("setCompToCompMid", 3);
                }
                else if (tracker == pianoPosition)
                {
                    standPianoSFX.Play();
                    timeManagerScript.GetComponent<timeManagerPlayerhome>().passTime();
                    playerAnims.Play("Armature|Piano_To_Stand");
                    Invoke("setPianoToMid", 5);
                }
            }
            else if (furniture == "Door") {
                if (tracker == bedPosition) {
                    bedToDoor = true;
                } else if (tracker == compPosition) {
                    standComputerSFX.Play();
                    timeManagerScript.GetComponent<timeManagerPlayerhome>().passTime();
                    playerAnims.Play("Armature|Chair_To_Stand");                    chairAnims.Play("Armature|chairAnimate2");
                    Invoke("setCompToCompMid", 3);
                } else if (tracker == pianoPosition) {
                    standPianoSFX.Play();
                    timeManagerScript.GetComponent<timeManagerPlayerhome>().passTime();
                    playerAnims.Play("Armature|Piano_To_Stand");
                    Invoke("setPianoToMid", 5);
                }
            } else if (furniture == "Computer") {
                if (tracker == bedPosition) {
                    doorBedToCompMid = true;
                }
                else if (tracker == doorPosition) {
                    doorBedToCompMid = true;
                }
                else if (tracker == pianoPosition) {
                    standPianoSFX.Play();
                    timeManagerScript.GetComponent<timeManagerPlayerhome>().passTime();
                    playerAnims.Play("Armature|Piano_To_Stand");
                    Invoke("setPianoToComp", 5);
                }
            }
            else if (furniture == "Piano") {
                if (tracker == doorPosition) {
                    doorBedToPianoMid = true;
                } else if (tracker == bedPosition) {
                    doorBedToPianoMid = true;
                } else if (tracker == compPosition) {
                    standComputerSFX.Play();
                    timeManagerScript.GetComponent<timeManagerPlayerhome>().passTime();
                    playerAnims.Play("Armature|Chair_To_Stand");
                    chairAnims.Play("Armature|chairAnimate2");
                    Invoke("setCompToPianoMid", 3);
                }
            }
        }
    }
    void setCompToCompMid() {
        playerAnims.Play("Armature|Walk_Cycle");
        compToCompMid = true;
    }

    void setCompToPianoMid() {
        playerAnims.Play("Armature|Walk_Cycle");
        compToPianoMid = true;
    }

    void setPianoToComp() {
        playerAnims.Play("Armature|Walk_Cycle");
        pianoToCompMid = true;
    }

    void setPianoToMid() {
        playerAnims.Play("Armature|Walk_Cycle");
        pianoToMid = true;
    }

    void playGetUpAnim() {
        playerAnims.Play("Armature|Bed_To_Stand");
    }

    void setMovableTrue() {
        globalVariables.movable = true;
    }

    public void sleepAnimation(int action) {
        if ((globalVariables.time == 3 || globalVariables.time == 2) && globalVariables.dt.AddDays(globalVariables.day) != new DateTime(2017, 8, 31)) {
            playerAnims.Play("Armature|Stand_To_Bed");
            enterBedSFX.Play();
            if (action == 0) {
                timeManagerScript.GetComponent<timeManagerPlayerhome>().Invoke("NextDay", 5);
            } else {
                timeManagerScript.GetComponent<timeManagerPlayerhome>().Invoke("nextEvent", 5);
            }
            Invoke("playGetUpAnim", 6);
            exitBedSFX.PlayDelayed(6);
        }
    }
}