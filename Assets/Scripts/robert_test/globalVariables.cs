﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;
using System.Linq;


public static class globalVariables {

    // Data Lists
    public static List<emailClass.Email> emailList;

    public static bool movable;

    public static int savefile;
    public static int time;
    public static int day;
    public static bool daystart;
    public static Dictionary<int, int> importantEventsDict;
    public static int dayEvent; // "Event", "Rehersal", "Audition", "Performance"
    public static DateTime dt;
    public static bool datesetup;
    public static int rehearsalCount;
    public static float preparedness;
    public static float mainvol=1;
    public static float musicvol=1;
    public static float othervol=1;
    public static float concertvol=1;

    public static void SetOriginalDate(int y, int m, int d)
    {
        if (datesetup ==false) { 
            dt = new DateTime(y, m, d);
            importantEventsDict= new Dictionary<int, int>();
            datesetup = true;
            rehearsalCount = 0;
        }
    }

    public static Vector3 GetDateFromInt(double d)
    {
        Vector3 date;
        DateTime newdt = dt.AddDays((d));
        date.x = newdt.Day;
        date.y = newdt.Month;
        date.z = newdt.Year;
        return date;
    }
}
