﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.IO;



public class LoadBar : MonoBehaviour {

    private Dropdown dropdown;
	// Use this for initialization
	void Start () {
        dropdown = GetComponent<Dropdown>();
        dropdown.ClearOptions();
        string saveSlot = Application.streamingAssetsPath + "/SaveState.json";

        string saveJsonString = File.ReadAllText(saveSlot);
        saveStateClass.SaveState saveState = JsonUtility.FromJson<saveStateClass.SaveState>(saveJsonString);
        if (saveState.SaveStateValue>0)
        {
            List<string> dropdownoptions = new List<string>(saveState.SaveStateValue);

            for (int i = 1; i<= saveState.SaveStateValue; i++)
            {
                string path = Application.streamingAssetsPath + "/Save"+ i+ "/Player.json";
                string jsonString = File.ReadAllText(path);

                playerClass.Player player = JsonConvert.DeserializeObject<List<playerClass.Player>>(jsonString)[0];
                //string player = "coco";
                dropdownoptions.Add(player.FirstName+ " " + player.LastName);

            }
            dropdown.AddOptions(dropdownoptions);

        }

    }

    // Update is called once per frame
    void Update () {
		
	}
}
