﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using Newtonsoft.Json;



public class timeManagerPlayerhome : MonoBehaviour {

    public AudioSource lightSwitchSFX;
    public GameObject statFuncts;

    public GameObject[] dials;
    public GameObject[] lights;
    Vector3 today;
    public Text daytext;
    private string[] tod = { "Morning", "Afternoon", "Evening", "Night" };
    private string[] month = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    private Color[] col = { new Color(0.9411765f, 0.7058824f, 0.9137255f), Color.white, new Color(0.8f, 0.372549f, 0.372549f), new Color(0.1215686f, 0.1254902f, 0.6313726f) };

    private dateClass.Date dateData;

    void Awake() {
        // print("AWAKE --> " + globalVariables.time);
        /*
        if (globalVariables.loadGame) {
            string path = Application.streamingAssetsPath + "/Temp/Date.json";
            string jsonString = File.ReadAllText(path);

            if (jsonString == "")
            {
                globalVariables.SetOriginalDate(2017, 8, 1);
            }
            else
            {
                dateData = JsonConvert.DeserializeObject<dateClass.Date>(jsonString);
                globalVariables.time = dateData.TimeOfDay;
                globalVariables.day = dateData.Day;
                globalVariables.daystart = dateData.DayStart;
                globalVariables.importantEventsDict = dateData.ImportantEventsDict;
                globalVariables.dt = dateData.CalendarDate;
                globalVariables.dayEvent = dateData.DayEvent;
                globalVariables.rehearsalCount = dateData.RehearsalCount;
            }
        }*/
    }

    // Use this for initialization
    void Start () {
        for (int i=0; i<dials.Length; i++) {
            lights[i].SetActive(false);
            dials[i].SetActive(false);
        }

        
        UpdateTime();
        //Debug.Log(globalVariables.time);
    }

    public void NextDay() {
        if (globalVariables.time == 3 || globalVariables.time == 2) {
            globalVariables.time = 0;
            globalVariables.day++;
            lights[3].SetActive(false);
            dials[3].SetActive(false);
            globalVariables.daystart = false;
            UpdateTime();
        }
    }

    public void nextEvent() {
        if (globalVariables.time == 3 || globalVariables.time == 2 && globalVariables.importantEventsDict.Count != 0) {
            globalVariables.time = 0;
            int dayholder = globalVariables.day;
            int newday = globalVariables.importantEventsDict.Keys.Min();
            if (newday <= globalVariables.day ) {
                globalVariables.day = newday;
                globalVariables.dayEvent = globalVariables.importantEventsDict[newday];
                globalVariables.importantEventsDict.Remove(newday);
            }
            lights[3].SetActive(false);
            dials[3].SetActive(false);
            globalVariables.daystart = false;
            UpdateTime();
        }

    }

    public void UpdateTime() {
        lights[globalVariables.time].SetActive(true);
        dials[globalVariables.time].SetActive(true);
        today = globalVariables.GetDateFromInt(globalVariables.day);
        daytext.text = month[(int)today.y - 1] + " " + (int)today.x + ", " + (int)today.z;
        RenderSettings.ambientLight = col[globalVariables.time];
        save();
    }

    public void passTime() {
        if (globalVariables.time < 3) {
            if (globalVariables.time == 3) {
                lightSwitchSFX.Play();
            }
            lights[globalVariables.time].SetActive(false);
            dials[globalVariables.time].SetActive(false);
            globalVariables.time += 1;
            UpdateTime();
        }
    }

    void save() {
        dateData = new dateClass.Date(globalVariables.time, globalVariables.day, globalVariables.daystart, globalVariables.importantEventsDict, globalVariables.dt, globalVariables.dayEvent, globalVariables.rehearsalCount);
        statFuncts.GetComponent<statsFunctions>().dateJSONSave(dateData);
    }
}
