﻿using UnityEngine;
using System;
using System.Collections;
using System.Globalization;
using UnityEngine.UI;

public class calendarScript : MonoBehaviour
{

    public GameObject options;
    public Text dateText;
    public Text dateText2;
    public GameObject smallmenu;
    public Text eventText;
    public Button[] Days;         //Holds 42 labels
    public Text[] DayLabels;
    public string[] Months;             //Holds the months
    public Text HeaderLabel;         //The label used to show the Month
    private string[] Events = { "Event", "Rehersal", "Audition", "Performance" };
    private int monthCounter;
    private int yearCounter = 0;

    private DateTime iMonth;
    private DateTime curDisplay;
    private int curday;
    private int curpos;

    void Start()
    {
        monthCounter = globalVariables.dt.AddDays(globalVariables.day).Month - 1;
        for (int i=0; i<42; i++)
        {
            DayLabels[i] = Days[i].transform.GetChild(0).GetComponent<Text>();
            DayLabels[i].fontSize = 30;
            int f = i;
            Days[i].onClick.AddListener(delegate { buttonPress(f); });
        }
        CreateMonths();
        CreateCalendar();
        if (globalVariables.day == 0) {
            addAuditionEvent();
        }
    }

    /*Adds al the months to the Months Array and sets the current month
    in the header label*/
    void CreateMonths()
    {
        Months = new string[12];
        iMonth = new DateTime(2000, 1, 1);

        for (int i = 0; i < 12; ++i)
        {
            iMonth = new DateTime(globalVariables.dt.AddDays(globalVariables.day).Year, i + 1, 1);
            Months[i] = iMonth.ToString("MMMM");
        }

        iMonth = new DateTime(globalVariables.dt.AddDays(globalVariables.day).Year, monthCounter+1, 1);


        HeaderLabel.text = Months[globalVariables.dt.AddDays(globalVariables.day).Month - 1] + " " + globalVariables.dt.AddDays(globalVariables.day).Year;
    }

    /*Sets the days to their correct labels*/
    void CreateCalendar()
    {
        curDisplay = iMonth;
        // start in week index
        int index = (int)curDisplay.DayOfWeek;

        for (int i=0; i<42;i++)
        {
            DayLabels[i].text = "";
            Days[i].image.color = Color.white;


        }

        while (curDisplay.Month == iMonth.Month)
        {
            if (curDisplay == globalVariables.dt.AddDays(globalVariables.day))
            {
                Days[index].image.color = Color.cyan;
            }
            
            if (globalVariables.importantEventsDict.ContainsKey((curDisplay-globalVariables.dt).Days))
            {
                Days[index].image.color = Color.magenta;

            }
            DayLabels[index].text = curDisplay.Day.ToString();
            curDisplay = curDisplay.AddDays(1);
            index++;
        }
    }

    /*when right arrow clicked go to next month */
    public void nextMonth()
    {
        monthCounter++;
        if (monthCounter > 11)
        {
            monthCounter = 0;
            yearCounter++;
        }

        HeaderLabel.text = Months[monthCounter] + " " + (globalVariables.dt.AddDays(globalVariables.day).Year + yearCounter);
        clearLabels();
        iMonth = iMonth.AddMonths(1);
        CreateCalendar();
    }

    /*when left arrow clicked go to previous month */
    public void previousMonth()
    {
        monthCounter--;
        if (monthCounter < 0)
        {
            monthCounter = 11;
            yearCounter--;
        }

        HeaderLabel.text = Months[monthCounter] + " " + (globalVariables.dt.AddDays(globalVariables.day).Year + yearCounter);
        clearLabels();
        iMonth = iMonth.AddMonths(-1);
        CreateCalendar();
    }

    /*clears all the day labels*/
    void clearLabels()
    {
        for (int x = 0; x < DayLabels.Length; x++)
        {
            DayLabels[x].text = null;
        }
    }

     void buttonPress(int i)
    {
        DateTime newdt = new DateTime((globalVariables.dt.AddDays(globalVariables.day).Year + yearCounter), monthCounter + 1, int.Parse(DayLabels[i].text));
        TimeSpan check = newdt- globalVariables.dt;
        int checkint = check.Days;
        curday = checkint;
        curpos = i;
        if (!globalVariables.importantEventsDict.ContainsKey(checkint)) { 
            smallmenu.SetActive(true);
            dateText2.text = Months[newdt.Month-1] + " " + newdt.Day + ", " + newdt.Year;

        }
        else
        {
            options.SetActive(true);
            dateText.text = Months[newdt.Month - 1] + " " + newdt.Day + ", " + newdt.Year;
            eventText.text = Events[globalVariables.importantEventsDict[checkint]];

        }
    }


    public void addEvent(int i)
    {
        if (!(i == 1 && globalVariables.rehearsalCount == 4))
        {
            globalVariables.importantEventsDict.Add(curday, i);
            Days[curpos].image.color = Color.green;
            if (i == 1) {
                globalVariables.rehearsalCount++;
            }
        }    
    }

    public void addPerformanceEvent() {
        globalVariables.importantEventsDict.Add(29, 3);
        Days[31].image.color = Color.magenta;
    }

    void addAuditionEvent() {
        globalVariables.importantEventsDict.Add(1, 2);
    }

    public void removeEvent()
    {
        globalVariables.importantEventsDict.Remove(curday);
        Days[curpos].image.color = Color.white;

    }
}