﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class enterBuilding : MonoBehaviour {

    string buildingname;
    string scenename;
    public GameObject button;
    public Text goodnight;
    public Text concertHallClosed;
    public Text practiceHallClosed;
    public Text txt;
	// Use this for initialization
	void Start () {


    }
	
	// Update is called once per frame
	void Update () {
    }


    public void SetName(string bname, string sname)
    {
        buildingname = bname;
        scenename = sname;
        txt.text = buildingname;
    }

    public string GetName()
    {
        return buildingname;
    
    }



    public void LoadScene()
    {
        SceneManager.LoadScene(scenename);
        gameObject.SetActive(false);
    }

}
