﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class optionPreference : MonoBehaviour {

    Slider slide;
    Slider slide2;
    Slider slide3;
    Slider slide4;

    public Slider slider;
    public Slider slider2;
    public Slider slider3;
    public Slider slider4;




    // Use this for initialization
    void Start () {
        slide = slider.GetComponent<Slider>();
        slide2 = slider2.GetComponent<Slider>();
        slide3 = slider3.GetComponent<Slider>();
        slide4 = slider4.GetComponent<Slider>();
        slide.value = globalVariables.mainvol;
        slide2.value = globalVariables.musicvol;
        slide3.value = globalVariables.othervol;
        slide4.value = globalVariables.concertvol;



    }

    // Update is called once per frame
    void Update () {
		
	}

    public void ChangeMainValue()
    {
        globalVariables.mainvol = slide.value;
    }

    public void ChangeMusicValue()
    {
        globalVariables.musicvol = slide2.value;
    }

    public void ChangeSoundValue()
    {
        globalVariables.othervol = slide3.value;
    }

    public void ChangeConcertValue()
    {
        globalVariables.concertvol = slide4.value;
    }

    public void SaveChanges()
    {
        PlayerPrefs.Save();
    }

    public void LoadLevel()
    {
        SaveChanges();
        SceneManager.LoadScene("titlescreen");
    }

}
