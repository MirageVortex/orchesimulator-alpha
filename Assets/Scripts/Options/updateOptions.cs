﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class updateOptions : MonoBehaviour
{

    AudioSource aud;

    // Use this for initialization
    void Start()
    {
        aud = GetComponent<AudioSource>();
        aud.volume = PlayerPrefs.GetFloat("volumeMain")* PlayerPrefs.GetFloat("volumeMusic");
    }

}
