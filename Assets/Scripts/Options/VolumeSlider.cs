﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio; 

public class VolumeSlider : MonoBehaviour {


    public AudioMixer mixer;

    public AudioSource mainSample;
    public AudioSource musicSample;
    public AudioSource soundEffectSample;
    public AudioSource concertSample;

    public void PlaySample(string name)
    {
        switch (name)
        {
            case "main":
                mainSample.Play();
                break;
            case "music":
                musicSample.Play();
                break;
            case "soundeffect":
                soundEffectSample.Play();
                break;
            case "concert":
                concertSample.Play();
                break;
        }
    }
	
	public void SetMainVol(float mainVol)
    {
        mixer.SetFloat("masterVolume", mainVol);
        PlayerPrefs.SetFloat("masterVolume", mainVol);
    }

    public void SetMusicVol(float musicVol)
    {
        mixer.SetFloat("musicVolume", musicVol);
        PlayerPrefs.SetFloat("musicVolume", musicVol);
    }

    public void SetSoundEffectVol(float sfxVol)
    {
        mixer.SetFloat("soundEffectVolume", sfxVol);
        PlayerPrefs.SetFloat("soundEffectVolume", sfxVol);
    }

    public void SetConcertVol(float concertVol)
    {
        mixer.SetFloat("concertVolume", concertVol);
        PlayerPrefs.SetFloat("concertVolume", concertVol);
    }


}
