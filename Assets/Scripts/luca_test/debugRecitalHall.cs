﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class debugRecitalHall : MonoBehaviour {

	public GameObject debugMenu;
	public bool debugMenuStatus;
	public GameObject forceExitButton;
	public string forceExitLocation;

	// Use this for initialization
	void Awake () {
		forceExitButton.SetActive (true);
		debugMenu.SetActive (false);
		debugMenuStatus = false;
	}

	// Detects debug key press
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (debugMenuStatus) {
				debugMenu.SetActive (false);
				debugMenuStatus = false;
			} else {
				debugMenu.SetActive (true);
				debugMenuStatus = true;
			}
		}
	}

	// Force exit the hall
	public void forceExit () {
		globalVariables.time = 2;
		SceneManager.LoadScene (forceExitLocation);
	}
}
