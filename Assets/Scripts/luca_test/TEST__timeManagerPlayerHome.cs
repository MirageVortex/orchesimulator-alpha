﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using Newtonsoft.Json;



public class TEST__timeManagerPlayerHome : MonoBehaviour
{

    public AudioSource lightSwitchSFX;
    public GameObject statFuncts;

    public GameObject[] lights;
    Vector3 today;
    public Text timetext;
    public Text daytext;
    public Image pointer;
    private string[] tod = { "Morning", "Afternoon", "Evening", "Night" };
    private string[] month = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    private Color[] col = { new Color(0.9411765f, 0.7058824f, 0.9137255f), Color.white, new Color(0.8f, 0.372549f, 0.372549f), new Color(0.1215686f, 0.1254902f, 0.6313726f) };
    private float[] positions = { 28.0f, 85.0f, 143.0f, 199.0f };

    private dateClass.Date dateData;

    // Use this for initialization
    void Start()
    {
        //Debug.Log(globalVariables.time);
        foreach (GameObject light in lights)
        {
            light.SetActive(false);
        }

        string path = Application.streamingAssetsPath + "/Temp/Date.json";
        string jsonString = File.ReadAllText(path);

        if (jsonString == "")
        {
            globalVariables.SetOriginalDate(2017, 8, 1);
        }
        else
        {
            dateData = JsonConvert.DeserializeObject<dateClass.Date>(jsonString);
            globalVariables.time = dateData.TimeOfDay;
            globalVariables.day = dateData.Day;
            globalVariables.daystart = dateData.DayStart;
            globalVariables.importantEventsDict = dateData.ImportantEventsDict;
            globalVariables.dt = dateData.CalendarDate;
            globalVariables.dayEvent = dateData.DayEvent;
            globalVariables.rehearsalCount = dateData.RehearsalCount;
        }
        UpdateTime();
        //Debug.Log(globalVariables.time);
    }

    public void NextDay()
    {
        if (globalVariables.time == 3 || globalVariables.time == 2)
        {
            globalVariables.time = 0;
            if (globalVariables.importantEventsDict.Count == 0)
            {
                globalVariables.day++;
            }
            else
            {
                int dayholder = globalVariables.day;
                if (globalVariables.importantEventsDict.Count != 0)
                {
                    int newday = globalVariables.importantEventsDict.Keys.Min();
                    // if (newday <= globalVariables.day ) {
                    globalVariables.day = newday;
                    globalVariables.dayEvent = globalVariables.importantEventsDict[newday];
                    globalVariables.importantEventsDict.Remove(newday);
                    // } else {

                    // }
                }
                //if (globalVariables.importantEventsDict.Count == 0) {
                //    globalVariables.day++;
                //}
            }
            lights[3].SetActive(false);
            globalVariables.daystart = false;
            UpdateTime();
        }
    }
    public void UpdateTime()
    {
        lights[globalVariables.time].SetActive(true);
        today = globalVariables.GetDateFromInt(globalVariables.day);
        timetext.text = tod[globalVariables.time];
        daytext.text = month[(int)today.y - 1] + " " + (int)today.x + ", " + (int)today.z;
        pointer.rectTransform.position = new Vector3(positions[globalVariables.time], pointer.rectTransform.position.y, 0);
        RenderSettings.ambientLight = col[globalVariables.time];
        save();
    }

    public void passTime()
    {
        if (globalVariables.time < 3)
        {
            if (globalVariables.time == 3)
            {
                lightSwitchSFX.Play();
            }
            lights[globalVariables.time].SetActive(false);
            globalVariables.time += 1;
            UpdateTime();
        }
    }

    void save()
    {
        dateData = new dateClass.Date(globalVariables.time, globalVariables.day, globalVariables.daystart, globalVariables.importantEventsDict, globalVariables.dt, globalVariables.dayEvent, globalVariables.rehearsalCount);
        statFuncts.GetComponent<statsFunctions>().dateJSONSave(dateData);
    }
}
