﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class songBehavior_luca : MonoBehaviour
{

	public Instruments[] inst;
	public float tick;
	private float timer;
	private int mistakes;
	[System.Serializable]
	public struct Instruments { 
		public GameObject instrument;
		public AudioSource aud;
		public UnityMidi.MidiPlayer mp;
		[Range(0,1)]
		public float chance;
		public bool playing;
	}
	// Use this for initialization
	void Start()
	{
		// Instantiates all audio, midi players, and chances
		for (int i = 0; i < inst.Length; i++)
		{
			inst[i].aud = inst[i].instrument.GetComponent<AudioSource>();
			inst[i].mp = inst[i].instrument.GetComponent<UnityMidi.MidiPlayer>();
			inst[i].chance = 0.9f;
		}

		// Plays the midi (goes through each instrument to play)
		for (int i = 0; i< inst.Length; i++)
		{
			inst[i].mp.Play();
		}
	}

	// Update is called once per frame
	void Update()
	{
		timer += Time.deltaTime;

		// Checks time between each tick to determine opportunity to have a mistake
		if (timer>tick)
		{
			for (int i = 0; i < inst.Length; i++)
			{
				inst[i].playing = true;
				if (Random.Range(0, 1f) > ((globalVariables.preparedness))/5f+0.8f) { 
					inst[i].aud.pitch = 1.05946f;
					mistakes++;
				}
				else if (inst[i].aud.pitch == 1.05946f)
				{
					inst[i].aud.pitch = 0.9438770694f;

				} else
				{
					inst[i].aud.pitch = 1;
				}
			}
			timer -= tick;

		}
	}
}
