﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class recitalHallController : MonoBehaviour {

	public GameObject songManagerObject;

	public GameObject skipButton;
	public GameObject confirmationBox;
	public string scene;

	public GameObject instrument;
	private float musicLength;


	// Initialization
	void Start () {
		confirmationBox.SetActive (false);
		skipButton.SetActive (true);
		Time.timeScale = 1;
	}

	// Any updating processes
	void Update () {
	}

// SKIP FUNCTIONALITY //
	// Skip Button function
	public void startSkipProcess () {
		skipButton.SetActive (false);
		confirmationBox.SetActive (true);
		Time.timeScale = 0;
		songManagerObject.GetComponent<SongManager> ().pauseMusic ();
	}

	// Confirmation Box functions
	public void confirmSkip () {
		confirmationBox.SetActive (false);
		globalVariables.time = 3;
		Time.timeScale = 1;
		SceneManager.LoadScene (scene);
	}

	public void cancelSkip () {
		confirmationBox.SetActive (false);
		skipButton.SetActive (true);
		Time.timeScale = 1;
		songManagerObject.GetComponent<SongManager> ().unpauseMusic ();
	}

// ----------------- // 
}
