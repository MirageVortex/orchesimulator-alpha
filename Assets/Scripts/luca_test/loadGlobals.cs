﻿using UnityEngine;
using System.IO;
using Newtonsoft.Json;

public class loadGlobals : MonoBehaviour {

    private dateClass.Date dateData;

    public void loadGlobalVariables() {
        string path = Application.streamingAssetsPath + "/Temp/Date.json";
        string jsonString = File.ReadAllText(path);

        dateData = JsonConvert.DeserializeObject<dateClass.Date>(jsonString);
        globalVariables.time = dateData.TimeOfDay;
        globalVariables.day = dateData.Day;
        globalVariables.daystart = dateData.DayStart;
        globalVariables.importantEventsDict = dateData.ImportantEventsDict;
        globalVariables.dt = dateData.CalendarDate;
        globalVariables.dayEvent = dateData.DayEvent;
        globalVariables.rehearsalCount = dateData.RehearsalCount;
    }
}
