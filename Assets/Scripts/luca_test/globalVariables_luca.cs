using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;
using System.Linq;


public static class globalVariables_luca {

	// Data Lists
	public static List<emailClass.Email> emailList;

	public static int time;
	public static int day;
	public static bool daystart;
	public static Dictionary<int, int> importantEventsDict;
	public static int dayEvent; // "Event", "Rehersal", "Audition", "Performance"
	public static DateTime dt;
	public static bool datesetup;
	public static int rehearsalCount;
	public static float preparedness;
	public static float mainvol;
	public static float musicvol;
	public static float othervol;
	public static float concertvol;
	public static string[] instrumentList = { "Conductor", "Flute", "Oboe", "Clarinet", "Bassoon", "Horn", "Trumpet",
											"Violin", "Viola", "Cello", "Double Bass", "Percussion"};
	public static string[] npcList = { "Frederick", "Brennen", "Hugo", "Elian", "Jaime", "Kobe", "Miles", "Kamren",
									"Chaim", "Aryan", "Collin", "Amir", "Hamza", "Jimmy", "Heath", "Aydin", "Rigoberto", "Daniel", "Xander",
									"Josue", "Kash", "Rhys", "Jayson", "Mark", "Gaven", "Timothy", "Chris", "Demetrius", "Kellen", "Trenton",
									"Avery", "Jon", "Patrick", "Antony", "Alejandro", "Jamarion", "Branden", "Adriel", "Marc", "Guillermo",
									"Lorenzo", "Cohen", "Marcel", "Case", "Adrian", "Jameson", "Davon", "Wilson", "Jack", "Keaton", "Brian",
									"Landyn", "Miguel", "Will", "Krish", "Marques", "Ezra", "Jovanni", "Isaiah", "Maximilian", "Kyler", "Amare",
									"Javier", "Russell", "Leo", "Tyler", "Alden", "Kaiden", "Toby", "Julien", "Jordon", "Jesus", "Ismael", "Javon",
									"Jackson", "Alexander", "Freddy", "Sebastian", "Raul", "Yahir", "Clarence", "Grayson", "Gael", "Ivan", "Rey",
									"Micah", "Ezequiel", "Zane", "Julius", "Esteban", "Simeon", "Adan", "Emanuel", "Justice", "Larry", "Skylar",
									"Jerome", "Kayden", "Cash", "Emery" };

	public static void SetOriginalDate(int y, int m, int d)
	{
		if (datesetup ==false) { 
			dt = new DateTime(y, m, d);
			importantEventsDict= new Dictionary<int, int>();
			datesetup = true;
			rehearsalCount = 0;
		}
	}

	public static Vector3 GetDateFromInt(double d)
	{
		Vector3 date;
		DateTime newdt = dt.AddDays((d));
		date.x = newdt.Day;
		date.y = newdt.Month;
		date.z = newdt.Year;
		return date;
	}
}
