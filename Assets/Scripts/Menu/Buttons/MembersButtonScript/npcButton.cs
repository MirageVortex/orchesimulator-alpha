﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class npcButton: MonoBehaviour {
    private string[] genderList;
    private string[] instrumentList;

    public Button button;
    public Text nameText;
    public Text instrumentText;
    public Text rankText;

    public charaClass.Character npc;
    private npcScrollList scrollList;

	// Use this for initialization
	void Start () {
        button.onClick.AddListener(HandleClick);
    }

    public void Setup(charaClass.Character currentNPC, string instrument, npcScrollList currentScrollList) {
        npc = currentNPC;
        nameText.text = npc.FirstName + ' ' + npc.LastName;
        instrumentText.text = instrument;
        rankText.text = npc.Rank.ToString();

        scrollList = currentScrollList;
    }

    private void HandleClick() {
        scrollList.openStatMenu(npc);
    }
}
