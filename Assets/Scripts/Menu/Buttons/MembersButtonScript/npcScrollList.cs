﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class npcScrollList : MonoBehaviour {

    public GameObject statsFunct;
    public GameObject itemListObj;
    public GameObject menuScriptObj;

    public GameObject hireButton;
    public GameObject statsMenu;

    string[] maleNames;
    string[] femaleNames;
    string[] lastNames;
    string[] genderList;
    string[] instrumentList;
    int[] instrumentListCount;

    public GameObject nameStatText;
    public GameObject genderStatText;
    public GameObject ageStatText;
    public GameObject instrumentStatText;

    public List<charaClass.Character> orchestraList;

    private charaClass.Character memberToHire;

    public Transform contentPanel;
    public npcScrollList otherNPCList;

    public SimpleObjectPool buttonObjectPool;

	// Use this for initialization
	void Start () {
        maleNames = itemListObj.GetComponent<itemList>().MaleNamesList;
        femaleNames = itemListObj.GetComponent<itemList>().FemaleNameList;
        lastNames = itemListObj.GetComponent<itemList>().LastNameList;
        genderList = itemListObj.GetComponent<itemList>().GenderList;
        instrumentList = itemListObj.GetComponent<itemList>().InstrumentList;
        instrumentListCount = itemListObj.GetComponent<itemList>().OrcheInstrumentList;

        // Load in current playerList
        if (this.name == "membersContent") {
            string path = Application.streamingAssetsPath + "/Temp/CharaStats.json";
            string jsonString = File.ReadAllText(path);

            if (jsonString == "") {
                orchestraList = new List<charaClass.Character>();
            } else {
                orchestraList = JsonConvert.DeserializeObject<List<charaClass.Character>>(jsonString);
            }
           
        } else {
            for (int i = 0; i < instrumentListCount.Length; i++) {
                for (int num = 0; num < instrumentListCount[i] * 2; num++) {
                    int firstName = Random.Range(0, 100);
                    int lastName = Random.Range(0, 26);
                    int Age = Random.Range(27, 51); //Random Age between 27 and 50
                    int Gender = Random.Range(0, 2);
                    int Rank = statsFunct.GetComponent<statsFunctions>().GetRank();
                    int Position = 0;

                    charaClass.Character npcToHire = statsFunct.GetComponent<statsFunctions>().CreateNPC(firstName, lastName, Age, Gender, i, Rank, Position);

                    orchestraList.Add(npcToHire);
                }
            }
        }

        refreshDisplay();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void refreshDisplay() {
        removeButtons();
        addButtons();
    }

    private void addButtons() {
        for (int i=0; i < orchestraList.Count; i++) {
            charaClass.Character npc = orchestraList[i];
            GameObject newButton = buttonObjectPool.GetObject();
            newButton.transform.SetParent(contentPanel, false);

            npcButton sampleButton = newButton.GetComponent<npcButton>();
            sampleButton.Setup(npc, instrumentList[npc.Instrument], this);
        }

    }

    private void removeButtons() {
        while (contentPanel.childCount > 0) {
            GameObject toRemove = transform.GetChild(0).gameObject;
            buttonObjectPool.ReturnObject(toRemove);
        }
    }

    public void hireNPC () {
        addMember(memberToHire, otherNPCList);
        removeMember(memberToHire, this);
        hireButton.SetActive(false);

        refreshDisplay();
        otherNPCList.refreshDisplay();
    }

    private void addMember(charaClass.Character npcToAdd, npcScrollList npcList) {
        npcList.orchestraList.Add(npcToAdd);
    }

    private void removeMember(charaClass.Character npcToDelete, npcScrollList npcList) {
        for (int i = npcList.orchestraList.Count - 1; i >= 0; i--) {
            if (npcList.orchestraList[i] == npcToDelete) {
                npcList.orchestraList.RemoveAt(i);
            }
        }
    }

    public void openStatMenu(charaClass.Character npc) {   
        if (this.name == "membersContent") {
            hireButton.gameObject.SetActive(false);
        } else {
            hireButton.gameObject.SetActive(true);
            memberToHire = npc;
        }
   
        menuScriptObj.GetComponent<menuToggle>().openMenu(statsMenu);
        UpdateMemberText(npc);      
    }

    private void UpdateMemberText(charaClass.Character npc) {
        nameStatText.GetComponent<Text>().text = npc.FirstName.ToString() + " " + npc.LastName.ToString();
        genderStatText.GetComponent<Text>().text = genderList[npc.Gender];
        string age = npc.Age.ToString() + " Years Old";
        ageStatText.GetComponent<Text>().text = age;
        instrumentStatText.GetComponent<Text>().text = instrumentList[npc.Instrument];
    }
}
