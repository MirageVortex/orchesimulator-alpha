﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class emailButton : MonoBehaviour {
    public Button button;

    public Text senderText;
    public Text subjectText;

    private emailClass.Email email;
    private emailScrollList scrollList;

    // Use this for initialization
    void Start() {
        button.onClick.AddListener(HandleClick);
    }

    public void Setup(emailClass.Email currentEmail, emailScrollList currentScrollList) {
        email = currentEmail;

        senderText.text = email.SenderName;
        subjectText.text = email.Subject;

        scrollList = currentScrollList;
    }

    private void HandleClick() {
        scrollList.openEmail(email);
    }
}
