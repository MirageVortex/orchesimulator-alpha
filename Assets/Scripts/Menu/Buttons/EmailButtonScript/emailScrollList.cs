﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class emailScrollList : MonoBehaviour{

    public GameObject itemListObj;
    public GameObject emailSection;
    public GameObject menuScript;

    public GameObject confirmationMenu;

    public Text senderText;
    public Text recipientText;
    public Text subjectText;
    public Text dateText;
    public Text mainText;

    public Transform contentPanel;
    // public npcScrollList otherNPCList;

    public SimpleObjectPool buttonObjectPool;

    private int emailCount;

    // Use this for initialization
    void Start() {
        if (globalVariables.emailList != null) {
            emailCount = globalVariables.emailList.Count;
            refreshDisplay();
        }
        
    }

    void Update() {
        if (emailCount != globalVariables.emailList.Count) {
            refreshDisplay();
            emailCount = globalVariables.emailList.Count;
        }
    }


    public void refreshDisplay() {
        removeButtons();
        addButtons();
    }

    private void addButtons() {
        for (int i = 0; i < globalVariables.emailList.Count; i++)
        {
            emailClass.Email email = globalVariables.emailList[i];
            GameObject newButton = buttonObjectPool.GetObject();
            newButton.transform.SetParent(contentPanel, false);

            emailButton sampleButton = newButton.GetComponent<emailButton>();
            sampleButton.Setup(email, this);
        }
    }

    private void removeButtons() {
        while (contentPanel.childCount > 0) {
            GameObject toRemove = transform.GetChild(0).gameObject;
            buttonObjectPool.ReturnObject(toRemove);
        }
    }

    public void openEmail(emailClass.Email email) {
        if (email.Type == 0 && globalVariables.emailList.Count == 2) {
            menuScript.GetComponent<menuToggle>().openMenu(confirmationMenu);
        }
        emailSection.SetActive(true);

        senderText.text = email.SenderName;
        recipientText.text = email.RecipientName;
        subjectText.text = email.Subject;
        dateText.text = email.Date;
        mainText.text = email.Content;
    }
}
