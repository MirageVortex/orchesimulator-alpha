﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.UI;

public class buyRepitoire : MonoBehaviour {
    public GameObject statFunctObj;
    public LibraryTest libraryMenuScript;

    public Text totalCostText;
    public Button addToCartButton;

    public Transform cartItems;

    public int totalCost;

    private List<LibraryClass.Repertoire> repertoireList;
    private List<LibraryClass.Repertoire> cart;
    private int[] purchasedIndexNumber;

    void Start () {
        string repitoirePath = Application.streamingAssetsPath + "/Temp/Library.json";

        string repitoireJSON = File.ReadAllText(repitoirePath);

        repertoireList = JsonConvert.DeserializeObject<List<LibraryClass.Repertoire>>(repitoireJSON);

        cart = new List<LibraryClass.Repertoire>();
    }

    void Update() {
        totalCostText.text = "$" + totalCost.ToString();

        if (repertoireList[0].Purchased){
            addToCartButton.interactable = false;
        }

        if (cart.Count != 0) {
            cartItems.gameObject.SetActive(true);
            for (int i = 0; i < cart.Count; i++) {
                cartItems.GetChild(i).GetChild(0).GetComponent<Text>().text = cart[i].Title;
                cartItems.GetChild(i).GetChild(1).GetComponent<Text>().text = cart[i].Composer;
                cartItems.GetChild(i).GetChild(2).GetComponent<Text>().text = "$" + cart[i].Price.ToString();
            }
        } else {
            cartItems.gameObject.SetActive(false);
        }
    }

    public void addToCart(int index) {
        cart.Add(repertoireList[index]);
        totalCost += repertoireList[index].Price;
    }

    public void removeFromCart(int index) {
        cart.Remove(repertoireList[index]);
        totalCost -= repertoireList[index].Price;
    }

    public void purchaseRepitoires() {
        if (cart.Count != 0) {
            for (int i = 0; i < cart.Count; i++) {
                repertoireList[i].Purchased = true;
                cart.Remove(cart[i]);
                totalCost = 0;
            }
        }
        libraryMenuScript.bought.text = "X";
        statFunctObj.GetComponent<statsFunctions>().repitoireJSONSave(repertoireList, 1);
    }

}
