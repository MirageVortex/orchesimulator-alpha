﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuFunctions : MonoBehaviour {

    public void openMenu (GameObject menu) {
        menu.SetActive(true);
    }

    public void closeMenu (GameObject menu) {
        menu.SetActive(false);
    }
}
