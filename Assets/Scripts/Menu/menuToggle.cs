﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menuToggle : MonoBehaviour {

    public AudioSource compMenuOpenSFX;
    public AudioSource compMenuCloseSFX;

    public GameObject computerObj;
    public GameObject bedObj;
    public GameObject doorObj;
    public GameObject pianoObj;

    public GameObject computerMenu;
    public GameObject bedMenu;
    public GameObject doorMenu;
    public GameObject managerMenu;
    public GameObject repitoireMenu;
    public GameObject auditionMenu;
    public GameObject buyMenu;
    public GameObject statsMenu;
    public GameObject playerStatsMenu;

    //public GameObject doorObj;
    //public GameObject bedObj;

    List<GameObject> toggleList;

	// Use this for initialization
	void Start () {
        computerMenu.SetActive(false);
        bedMenu.SetActive(false);
        doorMenu.SetActive(false);
        managerMenu.SetActive(false);
        repitoireMenu.SetActive(false);
        auditionMenu.SetActive(false);
        buyMenu.SetActive(false);
        statsMenu.SetActive(false);
        playerStatsMenu.SetActive(false);

        toggleList = new List<GameObject>();
    }
	
	// Update is called once per frame
	void Update () {
        if (toggleList.Count == 0) {
            computerObj.GetComponent<BoxCollider>().enabled = true;
            bedObj.GetComponent<BoxCollider>().enabled = true;
            doorObj.GetComponent<BoxCollider>().enabled = true;
            pianoObj.GetComponent<BoxCollider>().enabled = true;
        } else {
            computerObj.GetComponent<BoxCollider>().enabled = false;
            bedObj.GetComponent<BoxCollider>().enabled = false;
            doorObj.GetComponent<BoxCollider>().enabled = false;
            pianoObj.GetComponent<BoxCollider>().enabled = false;
        }

        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (toggleList.Count != 0) {
                closeMenu(toggleList[toggleList.Count - 1]);
            } else if (toggleList.Count == 0) {
                openMenu(playerStatsMenu);
            }
        }

        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray,out hit)) {
                toggleFurnitureMenus(hit.transform.name);
            }

        }
	}

    private void toggleFurnitureMenus(string objectName) {
        if (objectName == "Computer") {
            computerMenu.SetActive(true);
            bedMenu.SetActive(false);
            doorMenu.SetActive(false);
        } else if (objectName == "Door") {
            doorMenu.SetActive(true);
            bedMenu.SetActive(false);
            computerMenu.SetActive(false);
        } else if (objectName == "Bed") {
            bedMenu.SetActive(true);
            doorMenu.SetActive(false);
            computerMenu.SetActive(false);
        }
    }

    public void closeObjMenu(GameObject menu) {
        menu.SetActive(false);
    }

    public void openMenu (GameObject menu) {
        compMenuOpenSFX.Play();
        menu.SetActive(true);
        toggleList.Add(menu);
    }

    public void closeMenu (GameObject menu) {
        compMenuCloseSFX.Play();
        menu.SetActive(false);
        toggleList.Remove(menu);
    }
}
