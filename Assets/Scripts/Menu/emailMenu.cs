﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class emailMenu : MonoBehaviour {

    public int emailIndex;

    public Transform emailListObj;

    public GameObject senderNameText;
    public GameObject recipientNameText;
    public GameObject subjectText;
    public GameObject dateText;
    public GameObject bodyText;

    public List<emailClass.Email> emailList;

    // Use this for initialization
    void Start () {
        emailIndex = 0;

        string path = Application.streamingAssetsPath + "/Temp/Email.json";
        string jsonString = File.ReadAllText(path);

        emailList = JsonConvert.DeserializeObject<List<emailClass.Email>>(jsonString);
    }
	
	// Update is called once per frame
	void Update () {
        UpdateEmailMenu();
        UpdateEmailView();
	}

    void UpdateEmailMenu() {
        for (int i = 0; i < emailList.Count; i++) {
            emailListObj.GetChild(i).GetChild(0).GetComponent<Text>().text = emailList[i].SenderName;
            emailListObj.GetChild(i).GetChild(1).GetComponent<Text>().text = emailList[i].Subject;
        }
    }

    void UpdateEmailView() {
        senderNameText.GetComponent<Text>().text = emailList[emailIndex].SenderName;
        recipientNameText.GetComponent<Text>().text = emailList[emailIndex].RecipientName;
        subjectText.GetComponent<Text>().text = emailList[emailIndex].Subject;
        dateText.GetComponent<Text>().text = emailList[emailIndex].Date;
        bodyText.GetComponent<Text>().text = emailList[emailIndex].Content;
    }

    public void updateEmailIndex(int newIndex) {
        emailIndex = newIndex;
    }
}
