﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class libraryMenu : MonoBehaviour {
    public int libraryInt;

    //public Transform libraryListObj;

    //public GameObject compositionNameObj;

    public List<LibraryClass.Repertoire> libraryList;

	// Use this for initialization
	void Start () {
        string path = Application.streamingAssetsPath + "/Temp/Library.json";
        string jsonString = File.ReadAllText(path);

        libraryList = JsonConvert.DeserializeObject<List<LibraryClass.Repertoire>>(jsonString);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
