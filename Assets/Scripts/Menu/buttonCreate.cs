﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class buttonCreate : MonoBehaviour {

    public GameObject itemList;
    public GameObject statsFunct;
    public GameObject NPCButtonPrefab;

    public Transform Audition_List;
    public Transform NPC_List;

    private string[] maleNames;
    private string[] femaleNames;
    private string[] lastNames;
    private string[] genderList;
    private string[] instrumentList;
    private int[] instrumentListCount;

    private List<charaClass.Character> tempCharaList = new List<charaClass.Character>();
    private List<charaClass.Character> charaList;
    private List<playerClass.Player> playerList;

    public int charaIndex;
    public GameObject statsMenu;
    public GameObject auditionMenu;
    public Button hireButton;

    public GameObject nameText;
    public GameObject genderText;
    public GameObject ageText;
    public GameObject instrumentText;

    int yPosition = -20;
    // Use this for initialization
    void Start () {
        maleNames = itemList.GetComponent<itemList>().MaleNamesList;
        femaleNames = itemList.GetComponent<itemList>().FemaleNameList;
        lastNames = itemList.GetComponent<itemList>().LastNameList;
        genderList = itemList.GetComponent<itemList>().GenderList;
        instrumentList = itemList.GetComponent<itemList>().InstrumentList;
        instrumentListCount = itemList.GetComponent<itemList>().OrcheInstrumentList;

        string path = Application.streamingAssetsPath + "/Temp/CharaStats.json";
        string jsonString = File.ReadAllText(path);

        charaList = JsonConvert.DeserializeObject<List<charaClass.Character>>(jsonString);

        for (int i = 0; i < 5; i++) {
            Vector3 startingPosition = new Vector3(0, yPosition, 0);
            // print(startingPosition);
            GameObject NPCButton = Instantiate(NPCButtonPrefab, NPC_List);
            // NPCButton.transform.localPosition = startingPosition;
            NPCButton.transform.GetChild(0).GetComponent<Text>().text = charaList[i].FirstName + " " + charaList[i].LastName;
            NPCButton.transform.GetChild(1).GetComponent<Text>().text = instrumentList[charaList[i].Instrument];
            NPCButton.transform.GetChild(2).GetComponent<Text>().text = charaList[i].Rank.ToString();
            int num = i;
            NPCButton.GetComponent<Button>().onClick.AddListener(() => openCurrentStatMenu(num));
            yPosition -= 40;
        }
        float auditionYPosition = yPosition - 45;
        NPC_List.GetChild(0).localPosition = new Vector3(0, auditionYPosition, 0);

        
    }
	
	// Update is called once per frame
	void Update () {
        if (tempCharaList.Count != 0) {
            if (charaList.Contains(tempCharaList[charaIndex])) {
            hireButton.gameObject.SetActive(false);
            } else {
                hireButton.gameObject.SetActive(true);
            }
        }
		
	}

    public void createButton() {
        GameObject NPCButton = Instantiate(NPCButtonPrefab) as GameObject;
        NPCButton.transform.SetParent(NPC_List);
        NPCButton.transform.localPosition = new Vector3(0, yPosition, 0);
        // NPCButton.transform.localScale = new Vector3(.92f, .87f, 1);
        yPosition -= 30;
        int num = charaList.Count - 1;
        NPCButton.transform.GetChild(0).GetComponent<Text>().text = charaList[num].FirstName + " " + charaList[num].LastName;
        NPCButton.transform.GetChild(1).GetComponent<Text>().text = instrumentList[charaList[num].Instrument];
        NPCButton.transform.GetChild(2).GetComponent<Text>().text = charaList[num].Rank.ToString();
        
        NPCButton.GetComponent<Button>().onClick.AddListener(() => openCurrentStatMenu(num));

        float auditionYPosition = yPosition - 15f;
        NPC_List.GetChild(0).localPosition = new Vector3(0, auditionYPosition, 0);
    }

    public void HireNPC () {
        charaList.Add(tempCharaList[charaIndex]);
        statsFunct.GetComponent<statsFunctions>().orchestraJSONSave(charaList, 1);
        createButton();
    }

    public void openAuditionMenu () {
        int auditionYPosition = 2610;

        auditionMenu.SetActive(true);

        for (int i = 0; i < instrumentListCount.Length; i++) {
            for (int num = 0; num < instrumentListCount[i] * 2; num++) {
                int firstName = Random.Range(0, 100);
                int lastName = Random.Range(0, 26);
                int Age = Random.Range(27, 51); //Random Age between 27 and 50
                int Gender = Random.Range(0, 2);
                int Rank = statsFunct.GetComponent<statsFunctions>().GetRank();
                int Position = 0;

                tempCharaList.Add(statsFunct.GetComponent<statsFunctions>().CreateNPC(firstName, lastName, Age, Gender, i, Rank, Position));
            }
        }

        print(tempCharaList.Count);

        for (int i = 0; i < tempCharaList.Count; i++) {
            GameObject NPCButton = Instantiate(NPCButtonPrefab, Audition_List);
            NPCButton.transform.localScale = new Vector3(1, 1, 1);
            NPCButton.transform.GetChild(0).GetComponent<Text>().text = tempCharaList[i].FirstName + " " + tempCharaList[i].LastName;
            NPCButton.transform.GetChild(1).GetComponent<Text>().text = instrumentList[tempCharaList[i].Instrument];
            NPCButton.transform.GetChild(2).GetComponent<Text>().text = tempCharaList[i].Rank.ToString();
            int num = i;
            NPCButton.GetComponent<Button>().onClick.AddListener(() => openAuditionStatMenu(num));
            auditionYPosition -= 44;
        }

    }

    public void openAuditionStatMenu(int NewIndex) {
        charaIndex = NewIndex;
        statsMenu.SetActive(true);
        UpdateAuditionText();
    }

    public void openCurrentStatMenu (int NewIndex) {
        hireButton.gameObject.SetActive(false);
        charaIndex = NewIndex;
        statsMenu.SetActive(true);
        UpdateCurrentMemberText();
    }

    void UpdateAuditionText() {
        nameText.GetComponent<Text>().text = tempCharaList[charaIndex].FirstName.ToString() + " " + tempCharaList[charaIndex].LastName.ToString();
        genderText.GetComponent<Text>().text = genderList[tempCharaList[charaIndex].Gender];
        string age = tempCharaList[charaIndex].Age.ToString() + " Years Old";
        ageText.GetComponent<Text>().text = age;
        instrumentText.GetComponent<Text>().text = instrumentList[tempCharaList[charaIndex].Instrument];
    }

    void UpdateAuditionMenu() {
        for (int i = 0; i < tempCharaList.Count; i++) {
            if (Audition_List.GetChild(i).name.Substring(0, 3) == "NPC") {
                Audition_List.GetChild(i).GetChild(0).GetComponent<Text>().text = tempCharaList[i].FirstName;
                Audition_List.GetChild(i).GetChild(1).GetComponent<Text>().text = instrumentList[tempCharaList[i].Instrument];
                Audition_List.GetChild(i).GetChild(2).GetComponent<Text>().text = tempCharaList[i].Rank.ToString();
            }
        }
    }

    void UpdateCurrentMemberText() {
        nameText.GetComponent<Text>().text = charaList[charaIndex].FirstName.ToString() + " " + charaList[charaIndex].LastName.ToString();
        genderText.GetComponent<Text>().text = genderList[charaList[charaIndex].Gender];
        string age = charaList[charaIndex].Age.ToString() + " Years Old";
        ageText.GetComponent<Text>().text = age;
        instrumentText.GetComponent<Text>().text = instrumentList[charaList[charaIndex].Instrument];
    }




}
