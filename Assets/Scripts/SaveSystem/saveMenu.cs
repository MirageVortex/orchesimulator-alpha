﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class saveMenu : MonoBehaviour {

    public GameObject menu;
    public GameObject save_menu;

    // Use this for initialization
    void Start () {
        Time.timeScale = 1;
        save_menu.SetActive(false);
        menu.SetActive(false);
    
    }
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)){
            if (Time.timeScale == 0){
                Time.timeScale = 1;
                save_menu.SetActive(false);
                // print("game: resumed"); // for testing purposes;
            }
        }
    }

    void OnMouseDown()
    {
        if (Time.timeScale == 1 && !menu.activeSelf)
        {
            Time.timeScale = 0;
            // save_menu.SetActive(true);
        }
    }
}
