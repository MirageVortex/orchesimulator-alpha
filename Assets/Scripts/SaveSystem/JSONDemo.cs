﻿//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using UnityEngine;
//using Newtonsoft.Json;


//public class JSONDemo : MonoBehaviour {
//    string path;
//    string savepath;
//    string jsonString;

//    public GameObject InstrumentListObject;

//    public Dictionary<string, Character> InstrumentPlayers = new Dictionary<string, Character>(); // <Instrument Name, Character Class>

//    void Start () {

//        string[] InstrumentList = InstrumentListObject.GetComponent<instrumentList> ().InstrumentList;

//        path = Application.streamingAssetsPath + "/charaTest.json";
//        savepath = Application.streamingAssetsPath + "/charaTest.json";
//        jsonString = File.ReadAllText(path);

//        CharacterList characters = new CharacterList();
//        characters.CharaList = JsonConvert.DeserializeObject<List<Character>>(jsonString);

//        InstrumentPlayers = LoadDict(InstrumentPlayers, InstrumentList, characters);

//    }

//    private Dictionary<string, Character> LoadDict(Dictionary<string, Character> InstrumentDict, string[] InstrumentList, CharacterList chars)
//    {
//        for (int i = 0; i < chars.CharaList.Count; i++)
//        {
//            InstrumentDict.Add(InstrumentList[i], chars.CharaList[i]);
//        }

//        return InstrumentDict;
//    }

//    private void SaveToJson(CharacterList chars)
//    {
//        int local = (chars.CharaList.Count * 2) + 1;
//        string[] outputlist = new string[local];
//        string output;
//        int idk = 1;
//        outputlist.SetValue("[", 0);
//        for (int i = 0; i < chars.CharaList.Count; i++)
//        {
//            output = JsonConvert.SerializeObject(chars.CharaList[i]);
//            outputlist.SetValue(output, idk);
//            if (i != 29)
//            {
//                outputlist.SetValue(",", idk + 1);
//            }
//            idk += 2;
//        }
//        outputlist.SetValue("]", local - 1);

//        File.WriteAllLines(savepath, outputlist);
//    }

//}

//[System.Serializable]
//public class Character
//{
//    public int FirstName { get; set; }
//    public int LastName { get; set; }
//    public int Age { get; set; }
//    public int Gender { get; set; }
//    public int Instrument { get; set; }
//    public int[] Stats { get; set; } // Musicality, Experience, Charisma, Diligence, Prestige

//}

//[System.Serializable]
//public class CharacterList
//{
//    public List<Character> CharaList { get; set; }
//}
