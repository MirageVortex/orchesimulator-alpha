﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemList : MonoBehaviour {

    // Category of Instruments
    public string[] InstrumentList = { "Conductor", "Flute", "Oboe", "Clarinet", "Bassoon", "Horn", "Trumpet",
                                        "Violin", "Viola", "Cello", "Double Bass", "Percussion"};
    // ,"Piano", "Tuba", "Trombone", "Accordion", "Guitar", "Harp", "Soprano", "Mezzo", "Alto", "Tenor", "Baritone", "Bass" };
    // "Saxophone"

    // Instrument subcategories
    public string[] ConductorList = { "Conductor" };
    public string[] FluteList = { "Flute", "Piccolo", "Alto Flute" };
    public string[] OboeList = { "Oboe", "English Horn" };
    public string[] ClarinetList = { "Bb_Clarinet", "Eb_Clarinet", "A_Clarinet", "Bass_Clarinet"};
    // public string[] SaxophoneList = { "Alto_Saxophone", "Tenor_Saxophone", "Baritone_Saxophone"};
    public string[] BassoonList = { "Bassoon", "Contrabassoon" };
    public string[] HornList = { "Horn_1", "Horn_2", "Horn_3", "Horn_4" };
    public string[] TrumpetList = { "Bb_Trumpet", "C_Trumpet" };
    // public string[] TromboneList = { "Trombone", "Bass_Trombone" };
    // public string[] TubaList = { "Tuba", "Baritone_Horn" };
    public string[] ViolinList = { "Violin" };
    public string[] ViolaList = { "Viola" };
    public string[] CelloList = { "Cello" };
    public string[] DoubleBassList = { "DoubleBass" };
    public string[] PercussionList = { "Timpani" }; // "Percussion", "Mallets" 
    //public string[] PercussionList = { "Percussion", "Timpani", "Mallets" };
    // public string[] PianoList = { "Piano" };
    //public string[] AccordianList = { "Accordian" };
    //public string[] GuitarList = { "Guitar" };
    //public string[] HarpList = { "Harp" };
    //public string[] SopranoList = { "Accordian" };
    //public string[] MezzoList = { "Mezzo" };
    //public string[] AltoList = { "Alto" };
    //public string[] TenorList = { "Tenor" };
    //public string[] BaritoneList = { "Baritone" };
    //public string[] BassList = { "Bass" };

    // Gender List
    public string[] GenderList = { "Male", "Female" };

    // OrchestraInstrumentCount
    // 1 Conductor || 2 Flute || 2 Oboe || 2 Clarinet || 2 Bassoon || 4 Horns || 2 Trumpets
    // 1 Timpani || 22 Violins [12 First || 10 Second] || 9 Violas || 8 Cellos || 5 Double Bass
    // 59 NPCs + Player
    public int[] OrcheInstrumentList = {1, 2, 2, 2, 2, 4, 2, 22, 9, 8, 5, 1};

    // Male Names List
    public string[] MaleNamesList = { "Frederick", "Brennen", "Hugo", "Elian", "Jaime", "Kobe", "Miles", "Kamren",
        "Chaim", "Aryan", "Collin", "Amir", "Hamza", "Jimmy", "Heath", "Aydin", "Rigoberto", "Daniel", "Xander",
        "Josue", "Kash", "Rhys", "Jayson", "Mark", "Gaven", "Timothy", "Chris", "Demetrius", "Kellen", "Trenton",
        "Avery", "Jon", "Patrick", "Antony", "Alejandro", "Jamarion", "Branden", "Adriel", "Marc", "Guillermo",
        "Lorenzo", "Cohen", "Marcel", "Case", "Adrian", "Jameson", "Davon", "Wilson", "Jack", "Keaton", "Brian",
        "Landyn", "Miguel", "Will", "Krish", "Marques", "Ezra", "Jovanni", "Isaiah", "Maximilian", "Kyler", "Amare",
        "Javier", "Russell", "Leo", "Tyler", "Alden", "Kaiden", "Toby", "Julien", "Jordon", "Jesus", "Ismael", "Javon",
        "Jackson", "Alexander", "Freddy", "Sebastian", "Raul", "Yahir", "Clarence", "Grayson", "Gael", "Ivan", "Rey",
        "Micah", "Ezequiel", "Zane", "Julius", "Esteban", "Simeon", "Adan", "Emanuel", "Justice", "Larry", "Skylar",
        "Jerome", "Kayden", "Cash", "Emery" };

    // Female Names List
    public string[] FemaleNameList = { "Maya", "Iris", "Ashtyn", "Ashly", "Willow", "Kailyn", "Regan", "Tatum", "Kaila",
        "Madyson", "Naomi", "Brooklynn", "Monserrat", "Jamya", "Cassie", "Juliet", "Marisa", "Kaley", "Ada", "Maci",
        "Kadence", "Katelyn", "Aryanna", "Raquel", "Jamie", "Audrina", "Aspen", "Sloane", "Haylie", "Amber", "Chanel",
        "Kaitlyn", "Alicia", "Jaylynn", "Elle", "Lindsay", "Briana", "Alaina", "Sarahi", "Hazel", "Amiyah", "Claudia",
        "Kaleigh", "Rayna", "Jayla", "Cecilia", "Aaliyah", "Adrianna", "Chaya", "Liana", "Scarlet", "Delilah", "Addyson",
        "Andrea", "Heidi", "Eliza", "Serenity", "Isla", "Bailee", "Brylee", "Sydney", "Karma", "Aniya", "Itzel", "Lia",
        "Nancy", "Maggie", "Macie", "Kathy", "Daniella", "Caitlyn", "Isabela", "Khloe", "Anika", "Evie", "Amara", "Aimee",
        "Valeria", "Whitney", "Pamela", "Kathryn", "Ashleigh", "Yazmin", "Heidy", "Mira", "Cierra", "Paulina", "Lillie",
        "Denise", "Lilian", "Aniyah", "Ally", "Carlie", "Ellie", "Emely", "Laylah", "Jenny", "Angelina", "Kristin", "Genesis" };

    public string[] LastNameList = { "A.", "B.", "C.", "D.", "E.", "F.", "G.", "H.", "I.", "J.", "K.", "L.", "M.", "N.", "O.",
                                    "P.", "Q.", "R.", "S.", "T.", "U.", "V.", "W.", "X.", "Y.", "Z.", };

    public string[] Titles = { "Secretary of the City Council", "Manager of {0}", "Manager of {0}" };
}
