﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class rehearsalScript : MonoBehaviour {

	public GameObject eventWindow;
	public GameObject dialogueBox;
	public GameObject buttonSet1;
	public GameObject buttonSet2;
	public GameObject buttonSet3;

	public string orchestraMember;
	public string[] orchestraMemberGender;
	public int orchestraMemberGenderInt;

	public string orchestraInstrumentSection;

	private Dictionary<int, string> gameEventStrings;
	private Dictionary<int, string[]> gameEventSecondaryStrings;
	private Dictionary<int, string[]> gameEventOptions;

	private int eventID;
	private int[] eventIDList;

	private int eventFlag;

	private int qualityOfRehearsalInt;
	private string[] resultQualityGreat;
	private string[] resultQualityGood;
	private string[] resultQualityBad;

	private System.Action[] allButtonFunctions;

	private List<int> eventFlagsIntList;

	private float preparedness;

// Initializes all the Rehearsal Event dialogue
	void Awake () {
		// Instatiates the dictionaries and sets defaults
		gameEventStrings = new Dictionary<int, string> ();
		gameEventSecondaryStrings = new Dictionary<int, string[]> ();
		gameEventOptions = new Dictionary<int, string[]> ();
		orchestraMember = grabNPC ();
		orchestraMemberGenderInt = 0;
		orchestraInstrumentSection = grabSection ();
		if (orchestraMemberGenderInt == 0) {
			orchestraMemberGender = new string[] {"He", "he", "His", "his"};
		} else {
			orchestraMemberGender = new string[] { "She", "she", "Her", "her" };
		}
		eventFlag = 0;
		preparedness = globalVariables.preparedness;



		// Adds all events' dialogue to the dictionary string via ID number
		gameEventStrings.Add 
		(000, 
			"The orchestra rehearses without anything particular to note.");
		gameEventStrings.Add 
		(001, 
			"During rehearsal, " + orchestraMember + " continuously fiddles on his instrument whenever the orchestra is not playing. " +
			"It has reached a point where some of the other members are showing signs of visible discomfort. " +
			"If you confront him now, though, it would interrupt rehearsal and could strain your working relationship. " +
			"If you wait until after the rehearsal, the complaint might be forgotten by the next rehearsal.");
		gameEventStrings.Add
		(002,
			"Whether it’s due to lack of comprehension or inability to exaggerate, the " + orchestraInstrumentSection + " Section is not creating the changes " +
			"you want in the music. It would be possible to stop the orchestra’s rehearsal to try to fix the issue, but that would take an " +
			"undetermined amount of time. On the other hand, if this is not fixed, it is likely that by the performance, the " + orchestraInstrumentSection +
			" Section will still be playing it incorrectly.");



		// Adds all events' secondary dialogue (post option choice) to the dictionary via ID number
		gameEventSecondaryStrings.Add
		(000, new string[] {
			"null"});
		gameEventSecondaryStrings.Add
		(001, new string[] {
			"Situational",
			"Confront", "Confronting " + orchestraMember + " temporarily stops the orchestra, but as it overall increased the ensemble’s focus knowing they could be " +
			"called out, you think it might have been the most productive option. Hopefully " + orchestraMemberGender[1] + " won’t think too badly of you for it.",
			"Wait", "Catching " + orchestraMember + " after the rehearsal, you give a small lecture chastising " + orchestraMemberGender[3] + " fiddling. " + orchestraMemberGender[2] +
			" agrees to be more mindful in the next rehearsal.",
			"Ignore", "Since it hasn’t eclipsed your instructions yet, you decide to let it slide. Some of the others seemed a little irked couldn’t concentrate properly, " +
			"so you feel like you could have accomplished more on another day."});
		gameEventSecondaryStrings.Add
		(002, new string[] {
			"Conditional",
			"Great", "Luckily, it looks like you just needed to explain it in a different manner. The little transgression hardly takes up any time, and you easily continue the rehearsal.",
			"Good", "Correcting the mistakes took a bit of time, but the " + orchestraInstrumentSection + " Section was able to figure it out eventually. You sacrificed some " +
				"preparation time, but it seems worth the time. ",
			"Bad", "It really seems to be an issue of comprehension. You spend a large amount of time on trying to explain it, but the problem remains unsolved, and you are forced " +
				"to return to rehearsal with the problem unfixed.",
			"Ignore", "This problem would take too much time, and perhaps it will even be ironed out over the rehearsals to come. You decide to let it slide for now. "});



		// Adds all events' button options (and how many buttons) to the dictionary options via ID number
		gameEventOptions.Add
		(000, new string [] {
			"0"});
		gameEventOptions.Add
		(001, new string[] {
			"3", "Confront", "Wait", "Ignore"});
		gameEventOptions.Add
		(002, new string[] {
			"2", "Fix Now", "Ignore"});



		// Creates an array filled with event ID's
		eventIDList = gameEventStrings.Keys.ToArray ();



		// Creates different arrays regarding the quality of the rehearsal
		resultQualityGreat = new string[] {
			"The ensemble seems much more prepared after today."
		};
		resultQualityGood = new string[] {
			"The ensemble is sounding better."
		};
		resultQualityBad = new string[] {
			"There wasn't much progress today..."
		};



		// Creates an array of possible button functions
		allButtonFunctions = new System.Action[] {continueButton, conditional_Great, conditional_Good, conditional_Bad, conditional_Other, situational_1, situational_2, situational_3};



		// Creates a list that contains the eventFlag information for the possible buttons
		eventFlagsIntList = new List<int> ();
	}






// Incorporates content to the event window //
	// Misc Functions
	private bool isEven (int n) {
		if (n % 2 == 0) {
			return true;
		} else {
			return false;
		}
	}

	private int produceEvent () {
		//preparedness += 25;
		globalVariables.preparedness = preparedness;
		return Random.Range (0, eventIDList.Length);
	}



	// Creates Dialogue
	private void eventDialogue () {
		eventID = produceEvent ();
		string selectedEventDialogue = gameEventStrings [eventID];
		dialogueBox.GetComponent<Text> ().text = selectedEventDialogue;
	}

	// Determines which button set is active and inputs the necessary information into the buttons (functions, labeling, etc.)
	private void eventButtonSelector () {
		string[] buttonInformation = gameEventOptions [eventID];
		string numberOfButtons = buttonInformation [0];
		int buttonNumber = int.Parse (numberOfButtons);
		int childIteration = 0;
		int buttonNameIndex = 1;
		if (buttonNumber != 0) {
			// Assuming 1 good - 1 bad option
			if (buttonNumber == 2) {
				int eventFlagInt = 1;
				int buttonCounter = 0;
				buttonSet2.SetActive (true);
				Transform[] usableButtons = buttonSet2.GetComponentsInChildren<Transform> ();
				foreach (Transform button in usableButtons) {
					if (childIteration != 0) {
						if (isEven (childIteration) != true) {
							if (buttonCounter == 0) {
								button.GetComponent<Button> ().onClick.AddListener (delegate {
									allButtonFunctions [1].Invoke ();
									buttonSet2.SetActive (false);
									buttonSet1.SetActive (true);
								});
							} else {
								button.GetComponent<Button> ().onClick.AddListener (delegate {
									allButtonFunctions [4].Invoke ();
									buttonSet2.SetActive (false);
									buttonSet1.SetActive (true);
								});
							}
							eventFlagInt++;
						} else {
							button.GetComponent<Text> ().text = buttonInformation [buttonNameIndex];
							buttonNameIndex++;
						}
						childIteration++;
					} else {
						childIteration++;
						continue;
					}
				}
				return;
			}
			// Assuming only 3 choices
			if (buttonNumber == 3) {
				int eventFlagInt = 5;
				int buttonCounter = 0;
				buttonSet3.SetActive (true);
				Transform[] usableButtons = buttonSet3.GetComponentsInChildren<Transform> ();
				foreach (Transform button in usableButtons) {
					if (childIteration != 0) {
						if (isEven (childIteration) != true) {
							eventFlagsIntList.Add (eventFlagInt);
							if (buttonCounter == 0) {
								button.GetComponent<Button> ().onClick.AddListener (delegate {
									allButtonFunctions [5].Invoke ();
									buttonSet3.SetActive (false);
									buttonSet1.SetActive (true);
								});
							} 
							if (buttonCounter == 1) {
								button.GetComponent<Button> ().onClick.AddListener (delegate {
									allButtonFunctions [6].Invoke ();
									buttonSet3.SetActive (false);
									buttonSet1.SetActive (true);
								});
							} 
							if (buttonCounter == 2) {
								button.GetComponent<Button> ().onClick.AddListener (delegate {
									allButtonFunctions [7].Invoke ();
									buttonSet3.SetActive (false);
									buttonSet1.SetActive (true);
								});
							}
							eventFlagInt++;
							buttonCounter++;
						} else {
							button.GetComponent<Text> ().text = buttonInformation [buttonNameIndex];
							buttonNameIndex++;
						}
						childIteration++;
					} else {
						childIteration++;
						continue;
					}
				}
				return;
			}
		} else {
			buttonSet1.SetActive (true);
			}
	}

	// Randomizes good results
	private string getRandomGood() {
		int n = Random.Range (1, 4);
		if (n == 1) {
			return "Great";
		}
		if (n == 2) {
			return "Good";
		} else {
			return "Bad";
		}
	}

	// Creates the follow-up dialogue after a button has been pressed
	private void eventDialogueSecondary () {
		string[] eventDialogueSecondaryOptions;
		Dictionary<string, string> eventBranches = new Dictionary<string, string> ();
		eventDialogueSecondaryOptions = gameEventSecondaryStrings [eventID];
		if (eventDialogueSecondaryOptions [0] == "Conditional") {
			string randomGood = getRandomGood ();
			for (int i = 1; i < eventDialogueSecondaryOptions.Length ; i += 2) {
				eventBranches.Add (eventDialogueSecondaryOptions [i], eventDialogueSecondaryOptions [i + 1]);
			}
			if (eventFlag == 1) {
				dialogueBox.GetComponent<Text> ().text = eventBranches [randomGood];
				return;
			}
			if (eventFlag == 2) {
				dialogueBox.GetComponent<Text> ().text = eventBranches [randomGood];
				return;
			}
			if (eventFlag == 3) {
				dialogueBox.GetComponent<Text> ().text = eventBranches ["Bad"];
				return;
			}
			if (eventFlag == 4) {
				dialogueBox.GetComponent<Text> ().text = eventBranches ["Ignore"];
				return;
			}
		}
		if (eventDialogueSecondaryOptions [0] == "Situational") {
			int counter = 1;
			for (int i = 1 ; i < eventDialogueSecondaryOptions.Length ; i += 2) {
				eventBranches.Add (counter.ToString (), eventDialogueSecondaryOptions[i + 1]);
				counter++;
			}
			if (eventFlag == 5) {
				dialogueBox.GetComponent<Text> ().text = eventBranches ["1"];
				return;
			}
			if (eventFlag == 6) {
				dialogueBox.GetComponent<Text> ().text = eventBranches ["2"];
				return;
			}
			if (eventFlag == 7) {
				dialogueBox.GetComponent<Text> ().text = eventBranches ["3"];
				return;
			}
		}
	}

	// Deactivates the Event Window
	private void deactivateEventWindow () {
		eventWindow.SetActive (false);
	}
		





// Button Sets
// 0 = Continue
// 1 = Conditional:Great	2 = Conditional:Good	3 = Conditional:Bad		4 = Conditional:Other
// 5 = Situational:1		6 = Situational:2		7 = Situational:3
	private void continueButton () {
		eventFlag = 0;
		eventDialogueSecondary ();
	}

	private void conditional_Great () {
        globalVariables.preparedness += 0.25f;
		eventFlag = 1;
		eventDialogueSecondary ();
	}

	private void conditional_Good () {
        globalVariables.preparedness += 0.2f;
		eventFlag = 2;
		eventDialogueSecondary ();
	}

	private void conditional_Bad () {
        globalVariables.preparedness += 0.1f;
		eventFlag = 3;
		eventDialogueSecondary ();
	}

	private void conditional_Other () {
        globalVariables.preparedness += 0.15f;
		eventFlag = 4;
		eventDialogueSecondary ();
	}

	private void situational_1 () {
        globalVariables.preparedness += 0.25f;
		eventFlag = 5;
		eventDialogueSecondary ();
	}

	private void situational_2 () {
        globalVariables.preparedness += 0.20f;
		eventFlag = 6;
		eventDialogueSecondary ();
	}

	private void situational_3 () {
        globalVariables.preparedness += 0.15f;
		eventFlag = 7;
		eventDialogueSecondary ();
	}






// Quality of resulting practice
// 0: Great		1: Good		2: Bad
	private string getResultQuality () {
		if (qualityOfRehearsalInt == 0) {
			return resultQualityGreat[Random.Range (0, resultQualityGreat.Length)];
		};
		if (qualityOfRehearsalInt == 1) {
			return resultQualityGood[Random.Range (0, resultQualityGood.Length)];
		};
		if (qualityOfRehearsalInt == 2) {
			return resultQualityBad[Random.Range (0, resultQualityBad.Length)];
		};
		return "Default";
	}
	
	private void resultDialogueCreator () {
		string dialogue = getResultQuality ();
	}






// Continue-Event window interaction (closes Event Window)
	public void closeEventWindow () {
		eventWindow.SetActive (false);
		print ("Old Time " + globalVariables.time.ToString());
		if (globalVariables.time == 0) {
			globalVariables.time = 2;
		} else {
			globalVariables.time = 3;
		}
		print ("New Time " + globalVariables.time.ToString());
		SceneManager.LoadScene ("playerHome_elton");
	}






// Grabs NPC Name to use in events
	private string grabNPC () {
		return globalVariables_luca.npcList[Random.Range(1, globalVariables_luca.npcList.Length)];
	}






// Grabs a random orchestra section for use in events
	private string grabSection () {
		return globalVariables_luca.instrumentList[Random.Range(1, globalVariables_luca.instrumentList.Length)];
	}





// Event Compiling Functions
	public void eventCreator () {
		eventDialogue ();
		eventButtonSelector ();
	}

	// Creates even on script start
	void Start () {
		eventCreator ();
	}
}
