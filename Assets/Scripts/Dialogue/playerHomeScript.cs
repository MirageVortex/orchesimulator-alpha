﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerHomeScript : MonoBehaviour {

	public GameObject doorText;
	public GameObject bedText;
	public GameObject computerText;
	public GameObject pianoText;

	private int timeOfDay;

	public string gameEvent;
	public bool gameEventStatus;

	private string[] nullList;

	private string[] doorEnterAfternoon;
	private string[] doorEnterEvening;
	private string[] doorEnterNight;

	private string[] doorExitMorning;
	private string[] doorExitAfternoon;
	private string[] doorExitEvening;
	private string[] doorExitEveningEvent;
	private string[] doorExitAnytimeEvent;

	private string[] playerSleepMorning;
	private string[] playerSleepAfternoon;
	private string[] playerSleepEvening;
	private string[] playerSleepNight;
	private string[] playerSleepNightEvent;

	private string[] playerWakeUp;
	private string[] playerWakeUpEvent;

	private string[] computerOn;
	private string[] computerOnEmail;
	private string[] computerOff;

	private string[] pianoSit;
	private string[] pianoExit;



	// Initializes the dialogue into their respective lists
	void Awake () {
		// Sets gameEvent as a default event value
		gameEvent = "something";
		gameEventStatus = true;

		// Player Entering the Room (door)
		doorEnterAfternoon = new string[] { 
			"Alright, now that that's over with",
			"Not a bad start to the day.",
			"Some early morning productivity."
			};
		doorEnterEvening = new string[] {
			"I'd say that went well.",
			"A good day's work.",
			"Just a few more things to do now",
			"Well that was exhausting.",
			"What a tiresome day.",
			"Things could have gone better, but...",
			"Ah well."
			};
		doorEnterNight = new string[] { 
			"A long day, but a good day.",
			"Troublesome, but I'd say things went alright.",
			"I'd say that was okay.",
			"I'm about ready to drop.",
			"That was...",
			"... ...",
			"All's well that ends well, I guess.",
			"There are a couple things to work on.",
			"That was a little...frustrating.",
			"Gotta do something different next time.",
			"Haaah..."
			};

		// Player Exiting the room (door)
		doorExitMorning = new string[] { 
			"Nice weather this morning.",
			"Time for some early morning work.",
			"No time like the present!"
		};
		doorExitAfternoon = new string[] { 
			"Time to do some work!",
			"Nice day out.",
			"Its about time to go."
		};
		doorExitEvening = new string[] {
			"Looks like it'll be a pleasant evening.",
			"Hopefully things don't go too late.",
		};
		doorExitEveningEvent = new string[] { 
			"Just " + gameEvent + " left for today.",
			gameEvent + " time. I've been waiting all day for this.",
		};
		doorExitAnytimeEvent = new string[] { 
			"Hopefully " + gameEvent + " turns out goes well.",
			"Today we've got..." + gameEvent + ", right.",
			gameEvent + " today...I'm looking forward to it."
		};

		// Player enters the Bed
		playerSleepMorning = new string[] {
			"I just got up though.",
			"Isn't it a little too early?",
			"I'm not sleepy anymore..."
		};
		playerSleepAfternoon = new string[] {
			"There's still work to be done...",
			"I don't really have time for a nap right now..."
		};
		playerSleepEvening = new string[] { 
			"I guess I'll turn in early today.",
			"I've done what I need to today.",
			"It's a little early, but it should be alright."
		};
		playerSleepNight = new string[] { 
			"Finally get to sleep.",
			"It's been a long day, huh.",
			"*yawn*"
		};
		playerSleepNight = new string[] { 
			"I'm looking forward to " + gameEvent + " tomorrow..."
		};

		// Player wakes up and exits the Bed
		playerWakeUp = new string[] {
			"Another day.",
			"Seems like a good morning.",
			"Alright, time to get up.",
			"It's about time, isn't it.",
		};
		playerWakeUpEvent = new string[] { 
			gameEvent + " is today.",
			"I'm looking forward to " + gameEvent,
			"...I'm not sure about " + gameEvent
		};

		// Player sits at a Computer
		computerOn = new string[] {
			"Alright, what do we have here...",
			"So what I need to do is...",
			"Let's see...",
			"Hm..."
		};

		// Player sits at a Computer with a new Email
		computerOnEmail = new string[] {
			"Oh, a new email.",
			"What's this?",
			"Ah, something arrived.",
			"Let's take a look at this..."
		};

		// Player exits the Computer
		computerOff = new string[] {
			"Alright, that's enough for now...",
			"What else to do...",
			"Okay, now...",
			"Next would be..."
		};

		// Player sits at the Piano to play
		pianoSit = new string[] {
			"Let's play something.",
			"What to play...",
			"Hm..."
		};

		// Player stops playing the Piano
		pianoExit = new string[] {
			"Okay, that's enough of that.",
			"That was fun.",
			"Alright, now..."
		};
	}



	// Selected dialogue based on time of day and active events
	private string dialogueSelector (string[] eventList, string[] eventList2, string[] morningList, string[] afternoonList, string[] eveningList, string[] nightList) {
		if (gameEventStatus == true) {
			int useAnytimeEvent = Random.Range (0, 2);
			if (useAnytimeEvent != 1) {
				if (timeOfDay == 0) {
					if (gameEventStatus == true) {
						if (eventList2.Length != 0) {
							return eventList2 [Random.Range (0, eventList2.Length)];	
						}
					} else {
						return morningList [Random.Range (0, morningList.Length)];
					}
				}
				if (timeOfDay == 1) {
					return afternoonList [Random.Range (0, afternoonList.Length)];
				}
				if (timeOfDay == 2) {
					if (gameEventStatus == true) {
						if (eventList2.Length != 0) {
							return eventList2 [Random.Range (0, eventList2.Length)];	
						}
					} else {
						return eveningList [Random.Range (0, eveningList.Length)];
					}
				}
				if (timeOfDay == 3) {
					if (gameEventStatus == true) {
						if (eventList2.Length != 0) {
							return eventList2 [Random.Range (0, eventList2.Length)];	
						}
					} else {
						return nightList [Random.Range (0, nightList.Length)];
					}
				}	
			} else {
				return eventList [Random.Range (0, eventList.Length)];
			}
		} else {
			if (timeOfDay == 0) {
				return morningList [Random.Range (0, morningList.Length)];
			}
			if (timeOfDay == 1) {
				return afternoonList [Random.Range (0, afternoonList.Length)];
			}
			if (timeOfDay == 2) {
				return eveningList [Random.Range (0, eventList.Length)];
			}
			if (timeOfDay == 3) {
				return nightList [Random.Range (0, nightList.Length)];
			}
		}
		return "Dialogue not found";
	}



	// Dialogue selectors for different situations
	public void doorEnterSelector () {
		string selectedText;
		selectedText = dialogueSelector (nullList, nullList, nullList, doorEnterAfternoon, doorEnterEvening, doorEnterNight);
		doorText.GetComponent<Text> ().text = selectedText;
	}

	public void doorExitSelector () {
		string selectedText;
		selectedText = dialogueSelector (doorExitAnytimeEvent, doorExitEveningEvent, doorExitMorning, doorExitAfternoon, doorExitEvening, nullList);
		doorText.GetComponent<Text> ().text = selectedText;
	}

	public void playerSleepSelector () {
		string selectedText;
		selectedText = dialogueSelector (nullList, playerSleepNightEvent, playerSleepMorning, playerSleepAfternoon, playerSleepEvening, playerSleepNight);
		bedText.GetComponent<Text> ().text = selectedText;
	}

	public void playerWakeSelector () {
		string selectedText;
		selectedText = dialogueSelector (nullList, playerWakeUpEvent, playerWakeUp, nullList, nullList, nullList);
		bedText.GetComponent<Text> ().text = selectedText;
	}

	public void computerOnSelector () {
		string selectedText;
		selectedText = dialogueSelector (computerOnEmail, nullList, computerOn, computerOn, computerOn, computerOn);
		computerText.GetComponent<Text> ().text = selectedText;
	}

	public void computerOffSelector () {
		string selectedText;
		selectedText = dialogueSelector (nullList, nullList, computerOff, computerOff, computerOff, computerOff);
		computerText.GetComponent<Text> ().text = selectedText;
	}

	public void pianoSitSelector () {
		string selectedText;
		selectedText = dialogueSelector (nullList, nullList, pianoSit, pianoSit, pianoSit, pianoSit);
		pianoText.GetComponent<Text> ().text = selectedText;
	}

	public void pianoExitSelector () {
		string selectedText;
		selectedText = dialogueSelector (nullList, nullList, pianoExit, pianoExit, pianoExit, pianoExit);
		pianoText.GetComponent<Text> ().text = selectedText;
	}



	// Start for testing purposes
	void Start () {
		doorExitSelector ();
		playerSleepSelector ();
		computerOnSelector ();
		pianoSitSelector ();
	}

    void Update() 
    {
        //Debug.Log(globalVariables.time);
        timeOfDay = globalVariables.time;
    }
}