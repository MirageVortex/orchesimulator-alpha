﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class recitalHallScript : MonoBehaviour {

	public GameObject textLocation1;
	public GameObject textLocation2;
	public GameObject textLocation3;
	public GameObject textLocation4;
	public GameObject textLocation5;
	private GameObject[] textLocations;

	public int qualityOfPerformanceInt;
	private string[] performanceQualityGreat;
	private string[] performanceQualityGood;
	private string[] performanceQualityBad;

	private List<string> usedDialogue;






	// Sets performance dialogue and reviewer opinions
	void Awake () {
		// Sets default values
		qualityOfPerformanceInt = 0;



		// Sets performance audience responses into arrays
		performanceQualityGreat = new string[] {
			"...Beautiful...",
			"What an amazing performance...",
			"Bravo!",
			"Spectacular!",
			"That was spectacular!",
			"Perfect!",
			"I feel so moved...",
			"Outstanding!",
			"I...don't even know what to say...",
			"Oh my word...",
			"Wonderful show!"
		};
		performanceQualityGood = new string[] {
			"Very nice.",
			"A solid performance.",
			"I liked that!",
			"Wow!",
			"That was pretty good!",
			"Mm, I'm a fan.",
			"That was good!"
		};
		performanceQualityBad = new string[] {
			"...",
			"I want my money back!",
			"A waste of time.",
			"Disappointing.",
			"I...don't even know what to say...",
			"What a rip-off.",
			"Pathetic."
		};



		// Creates the GameObject array (for text locations)
		textLocations = new GameObject[] {
			textLocation1, textLocation2, textLocation3, textLocation4, textLocation5
		};
	}







	// Creates audience dialogues
	// 0: Great		1: Good		2: Bad
	private string getAudienceDialogue () {
		string dialogue;
		if (qualityOfPerformanceInt == 0) {
			while (true) {
				dialogue = performanceQualityGreat [Random.Range (0, performanceQualityGreat.Length)];
				if (usedDialogue.Contains (dialogue)) {
					continue;
				} else {
					usedDialogue.Add (dialogue);
					return dialogue;
				}
			}
		}
		if (qualityOfPerformanceInt == 1) {
			while (true) {
				dialogue = performanceQualityGood [Random.Range (0, performanceQualityGood.Length)];
				if (usedDialogue.Contains (dialogue)) {
					continue;
				} else {
					usedDialogue.Add (dialogue);
					return dialogue;
				}
			}
		}
		if (qualityOfPerformanceInt == 2) {
			while (true) {
				dialogue = performanceQualityBad [Random.Range (0, performanceQualityBad.Length)];
				if (usedDialogue.Contains (dialogue)) {
					continue;
				} else {
					usedDialogue.Add (dialogue);
					return dialogue;
				}
			}
		}
		return "Default";
	}

	private void audienceDialogeCreator () {
		usedDialogue = new List<string> {};
		foreach (GameObject textLocation in textLocations) {
			textLocation.GetComponent<Text> ().text = getAudienceDialogue ();
		}
	}



	// Dialogue compiling
	void Start () {
		audienceDialogeCreator ();
	}
}
