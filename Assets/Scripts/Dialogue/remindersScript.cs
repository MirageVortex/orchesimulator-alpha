﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class remindersScript : MonoBehaviour {

	public int timeOfDay;
	public string timeOfDayString;
	public string eventName;

	// Sets description dialogue
	void Awake () {
		eventName = "Default";

		interpretTimeOfDay ();
		string rehearsalReminder = "There's a rehearsal in the " + timeOfDayString + " today.";
		string concertReminder = "The concert is tonight.";
		string eventReminder = eventName + "is in the " + timeOfDayString + " today.";
	}

	private void interpretTimeOfDay () {
		if (timeOfDay == 0) {
			timeOfDayString = "Morning";
			return;
		}
		if (timeOfDay == 1) {
			timeOfDayString = "Afternoon";
			return;
		}
		if (timeOfDay == 2) {
			timeOfDayString = "Evening";
			return;
		}
		if (timeOfDay == 3) {
			timeOfDayString = "Night";
		}
	}
}
