﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class performanceReviewScript : MonoBehaviour {

	public int review1;
	public int review2;
	public int review3;
	public int review4;
	public int avgReview;
	private int[] reviews;

	private string[] reviewerOpinionGreat;
	private string[] reviewerOpinionGood;
	private string[] reviewerOpinionBad;

	private List<string> usedDialogue;

	void Awake () {
		// Sets default values
		review1 = review2 = review3 = review4 = avgReview = 0;
		reviews = new int[] {review1, review2, review3, review4};



		// Sets performance reviewer's opinions into arrays
		reviewerOpinionGreat = new string[] {
			"It was truly a remarkable performance.",
			"I can't wait to hear more.",
			"I was blown away.",
			"Spectacular."
		};
		reviewerOpinionGood = new string[] {
			"I quite enjoyed it.",
			"I think it was a good performance.",
			"Quite satisfying.",
			"Not yet world-class, but very promising"
		};
		reviewerOpinionBad = new string[] {
			"A disappointing display.",
			"Underwhelming.",
			"I admire their intentions, but it didn't meet expectations.",
			"Not at a professional level."
		};
	}






	// Creates reviewer reviews
	private string createOpinion (int review) {
		string dialogue;
		if (review >= 8) {
			while (true) {
				dialogue = reviewerOpinionGreat [Random.Range (0, reviewerOpinionGreat.Length)];
				if (usedDialogue.Contains (dialogue) != true) {
					usedDialogue.Add (dialogue);
					return dialogue;
				} else {
					continue;
				}
			}
		}
		if (review >= 5 & review < 8) {
			while (true) {
				dialogue = reviewerOpinionGood [Random.Range (0, reviewerOpinionGood.Length)];
				if (usedDialogue.Contains (dialogue) != true) {
					usedDialogue.Add (dialogue);
					return dialogue;
				} else {
					continue;
				}
			}
		}
		if (review < 5) {
			while (true) {
				dialogue = reviewerOpinionBad [Random.Range (0, reviewerOpinionBad.Length)];
				if (usedDialogue.Contains (dialogue) != true) {
					usedDialogue.Add (dialogue);
					return dialogue;
				} else {
					continue;
				}
			}
		}
		return "Default";
	}

	private void generateOpinions () {
		foreach (int review in reviews) {
			createOpinion (review);
		}
	}

	void Start () {
		generateOpinions ();
	}
}
