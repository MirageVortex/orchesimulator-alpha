﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buildingDescriptionsScript : MonoBehaviour {

	public string hallName;

	// Sets description dialogue
	void Awake () {
		hallName = "Default";

		string playerHomeDescription = "A simple studio apartment called home.";
		string practiceHallDescription = "The location the ensemble rents for rehearsals.";
		string recitalHallDescription = hallName + ", a fairly well known venue for musical performances.";
	}
}
