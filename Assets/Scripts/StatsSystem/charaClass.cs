﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class charaClass : MonoBehaviour {

    [System.Serializable]
    public class Character {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public int Gender { get; set; }
        public int Instrument { get; set; }
        public int[] Stats { get; set; } // Musicality, Experience, Charisma, Diligence, Prestige
        public int Rank { get; set; } // Rank 1 - 5 (1 = worst / 5 = best)
        public int Position { get; set; } // 0 - 2 (0 = normal / 1 = principal / 2 = concert master)

        public Character(string firstName, string lastName, int age, int gender, int instrument, int[] stats ,int rank, int position) {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            Gender = gender;
            Instrument = instrument;
            Stats = stats;
            Rank = rank;
            Position = position;
        }
    }
}
