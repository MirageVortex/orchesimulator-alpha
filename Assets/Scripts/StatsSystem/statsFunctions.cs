﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class statsFunctions : MonoBehaviour {

    public GameObject itemListObject;


    string[] InstrumentList;
    string[] maleNames;
    string[] femaleNames;
    string[] lastNames;

    bool debug = false;

    void Awake() {
        InstrumentList = itemListObject.GetComponent<itemList>().InstrumentList;
        maleNames = itemListObject.GetComponent<itemList>().MaleNamesList;
        femaleNames = itemListObject.GetComponent<itemList>().FemaleNameList;
        lastNames = itemListObject.GetComponent<itemList>().LastNameList;
    }

    public int GetRank() {
        // Generates a rank using the Random.Range function
        // 1-15 || 16-60 || 61-85 || 86-95 || 96-100
        int random_int = Random.Range(1, 101); // Cuz Max is exclusive
        
        if (random_int <= 15) {
            return 1;
        } else if (random_int > 15 && random_int <= 60) {
            return 2;
        } else if (random_int > 60 && random_int <= 92) {
            return 3;
        } else if (random_int > 93 && random_int <= 98) {
            return 4;
        } else {
            return 5;
        }
    }

    public int[] GenerateStats (int Rank) {
        // Generate stats based off of the Character's Rank
        //------------PRE ADJUSTED TOTALS------------------
        // 75 || 150 || 250 || 375 || 480
        //--------------- ADJUSTED ------------------------
        // 70 || 145 || 245 || 370 || 475
        int[] Stats = { 1, 1, 1, 1, 1 }; // Minimum Stats is 1;
        int TotalSP = 70; // Total Stat Points (initial)

        // Add more Total Stat Points based off of the Rank
        if (Rank == 2){
            TotalSP += 75;
        } else if (Rank == 3) {
            TotalSP += 175;
        } else if (Rank == 4) {
            TotalSP += 300;
        } else if (Rank == 5) {
            TotalSP += 405;
        }

        int points = 0; // Initialize the integar points variable
        // Start Distributing the points to the stats;
        while (TotalSP > 0) {
            for (int i = 0; i< 5; i++) {
                if (TotalSP > StatCap(Rank)) {
                    points = ConfigurePoints(Stats[i], Random.Range(0, StatCap(Rank)), Rank);
                    Stats[i] += points;
                    TotalSP -= points;
                } else {
                    points = ConfigurePoints(Stats[i], Random.Range(0, TotalSP+1), Rank);
                    Stats[i] += points;
                    TotalSP -= points;
                }
            }
        }
        return Stats;
    }

    public int ConfigurePoints (int StatPoints, int TempPoints, int Rank) {
        // Check if Total is greater than the current Stat Cap for the Rank
        int Total = StatPoints + TempPoints;
        if (Total > StatCap(Rank)) {
            int leftover = Total - StatCap(Rank);
            TempPoints -= leftover;
        }
        return TempPoints;
    }

    public int StatCap (int Rank) {
        // Stat Caps for character Ranks
        // 30 || 40 || 55 || 75 || 100
        if (Rank == 1) {
            return 25;
        } else if (Rank == 2) {
            return 40;
        } else if (Rank == 3) {
            return 60;
        } else if (Rank == 4) {
            return 80;
        } else {
            return 100;
        }
        
    }

    public charaClass.Character CreateNPC(int firstName, int lastName, int Age, int Gender, int Instrument, int Rank, int Position) {
        // Creates a random NPC Character Class.
        // Generate random stats
        int[] Stats = GenerateStats(Rank);

        // if male (use male names)
        // Create Character
        if (Gender == 0) {
            return new charaClass.Character(maleNames[firstName], lastNames[lastName], Age, Gender, Instrument, Stats, Rank, Position);
        // if female (use female names)
        } else {
            return new charaClass.Character(femaleNames[firstName], lastNames[lastName], Age, Gender, Instrument, Stats, Rank, Position);
        }
    }

    public void orchestraJSONSave(List<charaClass.Character> chars, int path) {
        // Set the save path to CharaStats.json in the StreamingAsset Folder (depending on path)
        // 0 = save1 (the perm save file) 1 = temp (temporary save file that will persist throughout game)
        string savePath;
        if (path == 0) {
            savePath = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/CharaStats.json";
        } else {
            savePath = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/CharaStats.json";
        }

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < chars.Count; i++) {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(chars[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void playerJSONSave(List<playerClass.Player> player, int path) {
        // Set the save path to CharaStats.json in the StreamingAsset Folder (depending on path)
        // 0 = save1 (the perm save file) 1 = temp (temporary save file that will persist throughout game)
        string savePath;
        if (path == 0) {
            savePath = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/Player.json";
        } else {
            savePath = Application.streamingAssetsPath + "/Temp/Player.json";
        }

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < player.Count; i++) {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(player[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void buildingJSONSave(List<buildingClass.Building> buildings) {
        // Set the save path to CharaStats.json in the StreamingAsset Folder
        string savePath = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/Building.json";

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < buildings.Count; i++) {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(buildings[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void repitoireJSONSave(List<LibraryClass.Repertoire> repitoires, int path) {
        // Set the save path to CharaStats.json in the StreamingAsset Folder (depending on path)
        // 0 = save1 (the perm save file) 1 = temp (temporary save file that will persist throughout game)
        string savePath;
        if (path == 0) {
            savePath = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/Library.json";
        } else {
            savePath = Application.streamingAssetsPath + "/Temp/Library.json";
        }

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < repitoires.Count; i++) {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(repitoires[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void emailJSONSave(List<emailClass.Email> emails, int path) {
        // Set the save path to CharaStats.json in the StreamingAsset Folder (depending on path)
        // 0 = save1 (the perm save file) 1 = temp (temporary save file that will persist throughout game)
        string savePath;
        if (path == 0) {
            savePath = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/Email.json";
        } else {
            savePath = Application.streamingAssetsPath + "/Temp/Email.json";
        }

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < emails.Count; i++) {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(emails[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void npcJSONSave(List<npcClass.NPC> npcs) {
        // Set the save path to CharaStats.json in the StreamingAsset Folder
        string savePath = Application.streamingAssetsPath + "/Save" + globalVariables.savefile + "/NPC.json";

        // Initialize the string for the JSON -> String Function.
        string JsonString = "[" + "\n";
        // Loops through the List of Characters
        for (int i = 0; i < npcs.Count; i++) {
            // Concatenates the Character information (converted to a string) along with the , and new line
            JsonString += JsonConvert.SerializeObject(npcs[i]) + "," + "\n";
        }
        // Trims away the extra "," at the end.
        JsonString.TrimEnd(',');
        JsonString += "]";

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void dateJSONSave(dateClass.Date date) {
        // Set the save path to Date.json in the StreamingAsset Folder
        string savePath = Application.streamingAssetsPath + "/Temp/Date.json";

        // Initialize the string for the JSON -> String Function.
        string JsonString = JsonConvert.SerializeObject(date);

        // Writes the JsonString Text into the file (save path)
        File.WriteAllText(savePath, JsonString);
    }

    public void allSave(List<charaClass.Character> chars, List<playerClass.Player> player, List<LibraryClass.Repertoire> repitoires,
        List<emailClass.Email> emails, int path) {
    
        orchestraJSONSave(chars, path);
        playerJSONSave(player, path);
        repitoireJSONSave(repitoires, path);
        emailJSONSave(emails, path);
    }

}
