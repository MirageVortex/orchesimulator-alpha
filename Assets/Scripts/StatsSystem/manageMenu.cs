﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class manageMenu : MonoBehaviour {

    public GameObject characterInfo;
    public Transform NPC_List;

    private List<charaClass.Character> charaList;
    private string[] InstrumentList;
    private string[] GenderList;
    private List<charaClass.Character> playerList;

    // Use this for initialization
    void Start()
    {
        // Get the instrument names and genders from the Instrument List and Gender List
        InstrumentList = characterInfo.GetComponent<itemList>().InstrumentList;
        GenderList = characterInfo.GetComponent<itemList>().GenderList;

        // Initiate the path to the JSON File
        string path = Application.streamingAssetsPath + "/CharaStats.json";
        // Read the JSON file and translate into the string.
        string jsonString = File.ReadAllText(path);

        // Convert the string into a player list containing Character Classes
        playerList = JsonConvert.DeserializeObject<List<charaClass.Character>>(jsonString);

        // For all the players in the list, change the player information on the management menu.
        for (int i = 0; i < NPC_List.childCount; i++)
        {
            NPC_List.GetChild(i).GetChild(0).GetComponent<Text>().text = playerList[i + 1].FirstName;
            NPC_List.GetChild(i).GetChild(1).GetComponent<Text>().text = InstrumentList[playerList[i + 1].Instrument];
            NPC_List.GetChild(i).GetChild(2).GetComponent<Text>().text = playerList[i + 1].Rank.ToString();
        }
    }
}
